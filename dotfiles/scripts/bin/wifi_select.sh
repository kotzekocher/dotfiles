#!/bin/bash
wifi_list=$(netctl list | sed s/\*//g)
echo ${wifi_list}
s=
for item in ${wifi_list}; do
    s="$s\n$item"
done
wifi=$(echo -e ${s} | rofi -dmenu)
if [ ! -z $wifi ]; then
    gksu netctl start $wifi
fi
