#!/bin/bash
set -x

profile=${1:-unknown}

_home3mon() {
    mon_top=DP1
    mon_bot=DP2
    mon_lap=eDP1
    i3-msg '[workspace="1:www"]' move workspace to output ${mon_bot}
    i3-msg '[workspace="2"]' move workspace to output ${mon_bot}
    i3-msg '[workspace="3"]' move workspace to output ${mon_top}
    i3-msg '[workspace="4"]' move workspace to output ${mon_bot}
    i3-msg '[workspace="5"]' move workspace to output ${mon_top}
    i3-msg '[workspace="6"]' move workspace to output ${mon_top}
    i3-msg '[workspace="7"]' move workspace to output ${mon_bot}
    i3-msg '[workspace="8"]' move workspace to output ${mon_top}
    i3-msg '[workspace="9:teams"]' move workspace to output ${mon_lap}
    i3-msg '[workspace="10:mail"]' move workspace to output ${mon_top}
}

_onemon() {
    mon_lap=eDP1
    i3-msg '[workspace=".*"]' move workspace to output ${mon_lap}
}

_office3mon() {
    mon_lap=eDP1
    mon_left=DP3-1
    mon_right=DP3-2
    i3-msg '[workspace="1:www"]' move workspace to output ${mon_left}
    i3-msg '[workspace="2"]' move workspace to output ${mon_left}
    i3-msg '[workspace="3"]' move workspace to output ${mon_right}
    i3-msg '[workspace="4"]' move workspace to output ${mon_right}
    i3-msg '[workspace="5"]' move workspace to output ${mon_left}
    i3-msg '[workspace="6"]' move workspace to output ${mon_left}
    i3-msg '[workspace="7"]' move workspace to output ${mon_right}
    i3-msg '[workspace="8"]' move workspace to output ${mon_right}
    i3-msg '[workspace="9:teams"]' move workspace to output ${mon_lap}
    i3-msg '[workspace="10:mail"]' move workspace to output ${mon_right}
}

case ${profile} in
    office3mon)_office3mon;;
    home3mon)_home3mon;;
    onemon)_onemon;;
    *)echo 'unknown'; exit 1;;
esac
