#!/bin/bash

select_cmd=$(zenity --title "Shutdown" --list --text "" --height=250 --column "action" "Lock" "Reboot" "Suspend" "Shutdown")

case ${select_cmd} in
    Lock) lock.sh;;
    Reboot) systemctl reboot;;
    Suspend) systemctl hibernate;;
    #Suspend) systemctl hybrid-sleep;;
    Shutdown) systemctl poweroff;;
esac
