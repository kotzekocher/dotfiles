#!/bin/bash


info() {
    mode=${1:short}
    if [[ ${mode} == short ]]; then
        activity=$(hamster current 2> /dev/null | cut -d " " -f 4)
    else
        activity=$(hamster current 2> /dev/null | cut -d " " -f 3- | sed 's/@.* / - /')
    fi
    if [ -n "$activity" ]; then
        echo "$activity"
    else
        echo "No Activity"
    fi
}

start_act() {
    hamster start 'General Activity@Work'
}

stop_act() {
    hamster stop
}

case $1 in
    info)
        info $2
        shift
        ;;
    start)start_act;;
    stop)stop_act;;
    *)exit 1;;
esac
