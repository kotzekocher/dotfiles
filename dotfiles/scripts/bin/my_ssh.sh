#!/bin/bash
ssh $@
exit 0
trap cleanup 2 3 6 15

cleanup() {
    kitty @ set-colors "background=#282A36"
}
if [ -n "$SSH_CONNECTION" ]; then
    ssh "$@"
else
    kitty @ set-colors "background=#4a1010"
    ssh "$@"
    cleanup
fi
