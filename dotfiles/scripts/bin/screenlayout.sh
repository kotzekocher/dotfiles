#!/bin/bash

restore_stuff() {
    sleep 0.5
    i3-msg restart
    sleep 1
    nitrogen --restore
}

# 1 mon
onemon() {
    xrandr --output VGA-0 --off
    xrandr --output DP-3 --off
    xrandr --output DP-3.8 --off
    xrandr --output DP-3.1.8 --off
    xrandr --output DP-3.2 --off
    xrandr --output DP-3.3 --off
    xrandr --output DP2 --off
    xrandr --output DP3 --off
    xrandr --output eDP1 --auto --pos 0x0 --size 1920x1200 --primary
}

threemon() {
    xrandr --output VGA-0 --off
    xrandr --output DP-3.8 --primary --mode 2560x1440 --pos 0x0
    xrandr --output DP-3.1.8 --mode 2560x1440 --pos 2560x0
    xrandr --output LVDS-0 --mode 1920x1080 --pos 640x1440
}

homeoffice() {
    xrandr --output VGA-0 --off
    xrandr --output eDP1 --primary --mode 1920x1200 --pos 0x860
    xrandr --output DP3 --mode 2560x1440 --pos 1920x860 --rotate normal
    xrandr --output DP2 --mode 2560x1440 --pos 4480x0 --rotate left

}

threemon_pivot() {
    xrandr --output DP3-1-8 --primary --mode 2560x1440 --pos 0x560 --rotate normal
    xrandr --output DP3-1-1-8 --mode 2560x1440 --pos 2560x0 --rotate left
    xrandr --output eDP1 --mode 1920x1200 --pos 640x2000
}

case $1 in
    1)onemon;;
    3)threemon;;
    3p)threemon_pivot;;
    ho)homeoffice;;
    *)echo '1,3,ho';exit 1;;
esac

restore_stuff
