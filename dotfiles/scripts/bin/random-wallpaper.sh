#!/bin/bash

#set -x
dir=~/pictures/wp
cd $dir || exit
files=(*)
random_wp="${files["$((RANDOM % ${#files[@]}))"]}"

feh --bg-scale "${dir}/${random_wp}"
