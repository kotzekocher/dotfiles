#!/usr/bin/python

from dbus.mainloop.glib import DBusGMainLoop
import dbus
#import gobject
from gi.repository import GObject as gobject
import logging
import re
import subprocess
from logging.handlers import RotatingFileHandler

logging.basicConfig()
logger = logging.getLogger(__name__)
log_file_handler = RotatingFileHandler('/tmp/spotify_adblock.log', maxBytes=1024000, backupCount=1)
log_file_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s %(message)s')
log_file_handler.setFormatter(log_file_formatter)
logger.addHandler(log_file_handler)
logger.setLevel(logging.DEBUG)


class Blocker:
    def __init__(self):
        self.session = dbus.SessionBus(mainloop=DBusGMainLoop(set_as_default=True))
        self.sink_index = '0'
        self.sink_muted = ''
        self.sinks = []
        self.get_pulse_index()
        logger.debug('pulse index: ' + self.sink_index + ' / muted: ' + self.sink_muted)

    def start_blocking(self):
        # monitor for changed propertis
        self.session.add_match_string_non_blocking("interface='org.freedesktop.DBus.Properties'")
        # self.session.add_match_string_non_blocking("type='signal',sender='org.freedesktop.DBus',interface='org.freedesktop.DBus',member='NameLost'")
        # self.session.add_match_string("interface='org.freedesktop.DBus',member='NameOwnerChanged'")
        self.session.add_message_filter(self.msg_callback)
        self.loop = gobject.MainLoop()
        self.loop.run()

    def block(self):
        for sink in self.sinks:
            idx, muted = self.get_sink_info(sink)
            logger.debug("blocking idx {}, with muted = {}".format(idx, muted))
            subprocess.call(['pactl', 'set-sink-input-mute', idx, '1'])

    def unblock(self):
        for sink in self.sinks:
            idx, muted = self.get_sink_info(sink)
            logger.debug("unblocking idx {}, with muted = {}".format(idx, muted))
            subprocess.call(['pactl', 'set-sink-input-mute', idx, '0'])

    def is_ad(self, data):
        artist = data.get('xesam:artist')[0]
        # title = data.get('xesam:title')
        url = data.get('xesam:url')
        if artist == '':
            logger.debug('empty artist')
            return True
        if '/ad/' in url:
            logger.debug('found /ad/')
            return True
        logger.debug('no ad found')
        return False

    def msg_callback(self, bus, message):
        logger.debug(message)
        if message.get_member() == "PropertiesChanged":
            sp = bus.get_object('org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2')
            itf = dbus.Interface(sp, 'org.freedesktop.DBus.Properties')
            data = itf.Get('org.mpris.MediaPlayer2.Player', 'Metadata')
            if self.is_ad(data):
                self.get_pulse_index()
                self.block()
            else:
                self.get_pulse_index()
                self.unblock()

    def get_sink_info(self, sink):
        try:
            info = re.findall(r"(index: [0-9]+|Mute: [yesno]+)", sink)
            sink_index = info[0].split(': ')[1]
            sink_muted = info[1].split(': ')[1]
            logger.debug('found spotify sink: {}'.format(sink_index))
            return sink_index, sink_muted
        except:
            logger.debug('no sink found. spotify probably not running.')

    def get_pulse_index(self):
        sink = ''
        self.sinks = []
        pa_out = subprocess.check_output(['pactl', 'list', 'sink-inputs']).decode('utf-8')
        for x in pa_out.split('Sink Input #'):
            if "Chromium" in x and "spotify" in x:
                sink = "index: " + x
                self.sinks.append(sink)
            elif "Spotify" in x:
                sink = "index: " + x
                self.sinks.append(sink)
        logger.debug(self.sinks)

if __name__ == "__main__":
    blocker = Blocker()
    blocker.start_blocking()
