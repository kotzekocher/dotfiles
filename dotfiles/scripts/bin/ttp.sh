#!/bin/bash
set -x
DEVICE='SynPS/2 Synaptics TouchPad'
state=$(xinput list-props "$DEVICE" | awk '/^\tDevice Enabled \([0-9]+\):\t[01]/ {print $NF}')
#state=$(synclient -l | awk '/TouchpadOff/ {print $3}')

if [ "${state}" = "1" ]; then
    xinput disable "SynPS/2 Synaptics TouchPad"
    notify-send "Touchpad OFF" ""
else
    xinput enable "SynPS/2 Synaptics TouchPad"
    notify-send "Touchpad ON" ""
fi
