#!/bin/bash

_lock() {
    args=""
    for output in $(swaymsg -t get_outputs| jq -r '.[]' | jq -r '.name'); do
        f="/tmp/screen_${output}.png"
        grim -o "$output" "${f}"
        #convert ${f} -blur "0x6" ${f}
        #corrupter -mag 2 -boffset 2 ${f} ${f}
        corrupter ${f} ${f}
        args="$args -i ${output}:${f}"
    done
    swaylock -k -l -c 000000 $args
}

case "$1" in
    lock)
        _lock
        ;;
    logout)
        i3-msg exit
        swaymsg exit
        ;;
    suspend)
        _lock && systemctl hibernate
        ;;
    reboot)
        systemctl reboot
        ;;
    poweroff)
        systemctl poweroff
        ;;
    *)
        _lock
esac
