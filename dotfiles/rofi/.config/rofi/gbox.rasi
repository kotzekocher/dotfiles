/* ==========================================================================
   Rofi color theme
   ========================================================================== */

* {
    /* Theme settings */
    highlight: bold italic;
    scrollbar: false;
	/* colors */
	fg:   #ebdbb2;
	bg:   #251f1fcc;
	color0:       #282828;
	color8:       #928374;
	color1:       #cc241d;
	color9:       #fb4934;
	color2:       #98971a;
	color10:      #b8bb26;
	color3:       #d79921;
	color11:      #fabd2f;
	color4:       #458588;
	color12:      #83a598;
	color5:       #b16286;
	color13:      #d3869b;
	color6:       #689d6a;
	color14:      #8ec07c;
	color7:       #a89984;
	color15:      #ebdbb2;

    /* Theme colors */
    background:                  @bg;
    /*background-color:            @bg;*/
    background-color:            transparent;
    foreground:                  @fg;
    border-color:                @color3;
    separatorcolor:              @bg;
    scrollbar-handle:            @color3;

    normal-background:           @bg;
    normal-foreground:           @fg;
    alternate-normal-background: @bg;
    alternate-normal-foreground: @fg;
    selected-normal-background:  @bg;
    selected-normal-foreground:  @color9;

    active-background:           @bg;
    active-foreground:           @fg;
    alternate-active-background: @active-background;
    alternate-active-foreground: @active-foreground;
    selected-active-background:  @active-background;
    selected-active-foreground:  @active-foreground;

    urgent-background:           @color1;
    urgent-foreground:           @color0;
    alternate-urgent-background: @color9;
    alternate-urgent-foreground: @color7;
    selected-urgent-background:  @bg;
    selected-urgent-foreground:  @fg;
}

window {
	transparency: "screenshot";
    background-color: @background;
    border:           0px;
	border-radius: 0px;
	border-color: @color4;
    padding:          10px;

}

mainbox {
    border:  0;
    padding: 0;
}

message {
    border:       0px 0 0;
    border-color: @separatorcolor;
    padding:      0px;
}

textbox {
    highlight:  @highlight;
    text-color: @foreground;
	border: 0;
}

listview {
    border:       0px;
    padding:      2px 0 0;
    border-color: @separatorcolor;
    spacing:      0px;
    scrollbar:    @scrollbar;
}

element {
    border:  0;
    padding: 0px;
}

element.normal.normal {
    text-color:         @normal-foreground;
    background:         @urgent-background;
    background-color:   transparent;
}

element.normal.urgent {
    text-color:         @urgent-foreground;
    background:         @urgent-background;
    background-color:   transparent;
}

element.normal.active {
    text-color:         @active-foreground;
    background:         @active-background;
    background-color:   transparent;
}

element.selected.normal {
    text-color:         @selected-normal-foreground;
    background:         @selected-normal-background;
    background-color:   transparent;
}

element.selected.urgent {
    background-color: @selected-urgent-background;
    text-color:       @selected-urgent-foreground;
}

element.selected.active {
    background-color: @selected-active-background;
    text-color:       @selected-active-foreground;
}

element.alternate.normal {
    text-color:         @alternate-normal-foreground;
    background:         @alternate-normal-background;
    background-color:   transparent;
}

element.alternate.urgent {
    text-color:         @alternate-urgent-foreground;
    background:         @alternate-urgent-background;
    background-color:   transparent;
}

element.alternate.active {
    text-color:         @alternate-active-foreground;
    background:         @alternate-active-background;
    background-color:   transparent;
}

scrollbar {
    width:        4px;
    border:       1;
    handle-color: @scrollbar-handle;
    handle-width: 8px;
    padding:      0;
}

sidebar {
    border:       1px 0 0;
    border-color: @separatorcolor;
}

inputbar {
    spacing:    0;
    text-color: @normal-foreground;
    padding:    0px;
    children:   [ prompt, textbox-prompt-sep, entry, case-indicator ];
}

case-indicator,
entry,
prompt,
button {
    spacing:    0;
    text-color: @normal-foreground;
}

button.selected {
    background-color: @selected-normal-background;
    text-color:       @selected-normal-foreground;
}

textbox-prompt-sep {
    expand:     false;
    str:        " > ";
    text-color: @normal-foreground;
    margin:     0 0 0 0;
}

/* vim:ft=css
