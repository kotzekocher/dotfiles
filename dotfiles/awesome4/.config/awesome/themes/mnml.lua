local awful         = require "awful"
local beautiful     = require "beautiful"
local gears         = require "gears"
local naughty       = require "naughty"
local wibox         = require "wibox"
local debug         = require "gears.debug"
local widgets       = require "kk.widgets_new"

awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.corner.nw,
}

-- applications

--terminal = "urxvtc"
terminal = "kitty --listen-on unix:/tmp/mykitty"
lock_command = "lock.sh"

beautiful.wallpapers = {
    "/home/chris/img/wallpaper.jpg",
}

clock = wibox.widget.textclock("<span color=\"" .. beautiful.xrdb.color3 .. "\"> [%a %d %b] [%H:%M]</span> ")

application_menu = {
    {"mail", "kitty --listen-on unix:/tmp/mykitty neomutt", functions.get_icon("email") },
    {"bluetooth", "blueman-manager", functions.get_icon("bluetooth") },
    {"gedit", "gedit", functions.get_icon("gedit") },
    {"terminal", "urxvtc", functions.get_icon("terminal"), show_in_launcher = true },
    {"firefox", "firefox-nightly", functions.get_icon("firefox-nightly") },
    {"chrome", "google-chrome-stable", functions.get_icon("google-chrome-stable") },
    {"nautilus", "nautilus", functions.get_icon("file-manager"), show_in_launcher = true },
    {"-----------------", "nil" },
    {"spotify", "spotify.sh", functions.get_icon("spotify"), show_in_launcher = true },
    {"deadbeef", "deadbeef", functions.get_icon("deadbeef"), show_in_launcher = true },
    {"vlc", "vlc", functions.get_icon("vlc"), show_in_launcher = true },
    {"pavucontrol", "pavucontrol", functions.get_icon("audio-equalizer"), show_in_launcher = true },
    {"steam", "steam", functions.get_icon("steam"), show_in_launcher = true },
    {"syncthing-gtk", "syncthing-gtk", functions.get_icon("syncthing") },
    { "zeug",
        {
           {"chrome-i", "google-chrome-stable --incognito", functions.get_icon("google-chrome-unstable")},
           {"ff-i", "firefox-nightly --private-window", functions.get_icon("firefox") },
        },
    },
    {"-----------------", "nil" },
    {"lock screen", lock_command, functions.get_icon("lock") },
    {"off", "my_reboot.sh" },
}

mainmenu = awful.menu({ items = application_menu})
menulauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mainmenu })

lo = awful.layout.layouts
screen_layout = {
    tags = {
        { names =  { "www", "mail", "emacs", "4", "5", "6", "7", "8",  "media" },
          layout = { lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2] },
        },
    }
}

awful.screen.connect_for_each_screen(function(s)
    awful.tag(screen_layout.tags[s.index].names, s, screen_layout.tags[s.index].layout)
    functions.set_wallpaper(s)
    s.bar = awful.wibar({ position = "top", screen = s, height = 14 })
    s.taglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons, { spacing = 0 })
    s.tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons, { spacing = 0} )
    s.mypromptbox = awful.widget.prompt()
    s.lobox = awful.widget.layoutbox(s)
    s.bar:setup{
        layout = wibox.layout.align.horizontal,
        first = "left",
        second = "left",
        third = "right",
        {
            layout = wibox.layout.fixed.horizontal,
            menulauncher,
            wibox.widget.systray(),
            widgets.widget_bar,
            --widgets.cpu_widget(),
            --widgets.net_widget(),
            --widgets.bat_widget("BAT1", "ACAD"),
            {
                widget = wibox.container.constraint,
                width = 500,
                s.tasklist,
            },
        },
        s.mypromptbox,
        {
            layout = wibox.layout.fixed.horizontal,
            s.taglist,
            clock,
            widgets.worldclock,
            s.lobox,
        },
    }
end)

s1 = 1
s2 = screen:count() > 1 and 2 or 1

custom_rules = {
    { rule = { class = "Google-chrome" }, properties = { tag = screen[s1].tags[1], switchtotag = true } },
    { rule = { class = "Nightly" }, properties = { tag = screen[s1].tags[1], switchtotag = true } },
    { rule = { class = "Chromium" }, properties = { tag = screen[s1].tags[1], switchtotag = true } },
    { rule = { class = "Thunderbird" }, properties = { tag = screen[s1].tags[2], switchtotag = true } },
    { rule = { class = "Deadbeef" }, properties = { tag = screen[s1].tags[9] } },
    { rule = { class = "Pavucontrol" }, properties = { tag = screen[s1].tags[9], floating = true } },
    { rule = { class = "Spotify" }, properties = { tag = screen[s1].tags[9] } },
    { rule = { class = "Steam" }, properties = { tag = screen[s1].tags[8], floating = true } },
    { rule = { class = "TeamSpeak 3" }, properties = { tag = screen[s1].tags[9], floating = true } },
    { rule = { class = "Emacs" }, properties = { tag = screen[s1].tags[3] } },
    { rule = { class = "URxvt" }, properties = { border_width = 1 } },
}

awful.rules.rules = awful.util.table.join(awful.rules.rules, custom_rules)
