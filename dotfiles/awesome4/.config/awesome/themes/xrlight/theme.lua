local xresources   = require("beautiful").xresources
local dpi          = xresources.apply_dpi
local shape        = require("gears.shape")
local theme_assets = dofile(theme_dir .. "/xresources/assets.lua")
local naughty      = require("naughty")
local functions      = require("kk.functions")

theme = {}
-- hack because xresources doesn't like my Xresources defines
xrdb = {}
xrdb.background = "#1d2021"
xrdb.foreground = "#ebdbb2"
--xrdb.color0 = "#282828"
xrdb.color0 = "#1d2021"
xrdb.color8 = "#928374"
xrdb.color1 = "#cc241d"
xrdb.color9 = "#fb4934"
xrdb.color2 = "#98971a"
xrdb.color10 = "#b8bb26"
xrdb.color3 = "#d79921"
xrdb.color11 = "#fabd2f"
xrdb.color4 = "#458588"
xrdb.color12 = "#83a598"
xrdb.color5 = "#b16286"
xrdb.color13 = "#d3869b"
xrdb.color6 = "#689d6a"
xrdb.color14 = "#8ec07c"
xrdb.color7 = "#a89984"
xrdb.color15 = "#ebdbb2"

theme.font          = "Roboto Mono for Powerline bold 8"
theme.xrdb = xrdb

theme.wallpapers = {}

theme.bg_normal     = xrdb.background
theme.bg_focus      = xrdb.color8
theme.bg_urgent     = xrdb.color1
theme.bg_minimize   = theme.bg_normal
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = xrdb.foreground
theme.fg_focus      = xrdb.color11
theme.fg_urgent     = xrdb.color0
theme.fg_minimize   = xrdb.color7

theme.useless_gap   = dpi(1)
theme.border_width  = 1
theme.floating_border_width  = 1
theme.border_normal = xrdb.color0
theme.border_focused = xrdb.color12
theme.border_marked = xrdb.color5

theme.taglist_fg_focus = xrdb.color11
theme.taglist_bg_focus = xrdb.color0
theme.taglist_fg_occupied = xrdb.color6
theme.taglist_bg_occupied = xrdb.color0
theme.taglist_fg_empty = xrdb.color15
theme.taglist_bg_empty = xrdb.color0
theme.taglist_fg_urgent = xrdb.color8
theme.taglist_bg_urgent = xrdb.color1

theme.tasklist_fg_normal = xrdb.color6
theme.tasklist_bg_normal = xrdb.color0
theme.tasklist_fg_focus = xrdb.color11
theme.tasklist_bg_focus = xrdb.color0
theme.tasklist_fg_urgent = xrdb.color15
theme.tasklist_bg_urgent = xrdb.color1
theme.tasklist_disable_icon = true
theme.tasklist_align = "center"

theme.widget_fg_normal = xrdb.color15
theme.widget_fg_green = xrdb.color2
theme.widget_fg_orange = xrdb.color1
theme.widget_fg_red = xrdb.color9
theme.widget_fg_blue = xrdb.color4

theme.widget_icon_bat0 = ""
theme.widget_icon_bat1 = ""
theme.widget_icon_bat2 = ""
theme.widget_icon_bat3 = " "
theme.widget_icon_bat4 = ""
theme.widget_icon_charge = ""
theme.widget_icon_pkg = ""
theme.widget_icon_ram = ""
theme.widget_icon_cpu = ""
theme.widget_icon_net = ""
theme.widget_icon_wifi = ""

theme.layout_fairh = theme_dir .. "/xresources/layouts/fairhw.png"
theme.layout_fairv = theme_dir .. "/xresources/layouts/fairvw.png"
theme.layout_floating  = theme_dir .. "/xresources/layouts/floatingw.png"
theme.layout_magnifier = theme_dir .. "/xresources/layouts/magnifierw.png"
theme.layout_max = theme_dir .. "/xresources/layouts/maxw.png"
theme.layout_fullscreen = theme_dir .. "/xresources/layouts/fullscreenw.png"
theme.layout_tilebottom = theme_dir .. "/xresources/layouts/tilebottomw.png"
theme.layout_tileleft   = theme_dir .. "/xresources/layouts/tileleftw.png"
theme.layout_tile = theme_dir .. "/xresources/layouts/tilew.png"
theme.layout_tiletop = theme_dir .. "/xresources/layouts/tiletopw.png"
theme.layout_spiral  = theme_dir .. "/xresources/layouts/spiralw.png"
theme.layout_dwindle = theme_dir .. "/xresources/layouts/dwindlew.png"
theme.layout_cornernw = theme_dir .. "/xresources/layouts/cornernww.png"
theme.layout_cornerne = theme_dir .. "/xresources/layouts/cornernew.png"
theme.layout_cornersw = theme_dir .. "/xresources/layouts/cornersww.png"
theme.layout_cornerse = theme_dir .. "/xresources/layouts/cornersew.png"

theme.titlebar_fg_normal = xrdb.color6
theme.titlebar_bg_normal = xrdb.color0
theme.titlebar_fg_focus = xrdb.color11
theme.titlebar_bg_focus = xrdb.color0
theme.titlebar_fg = theme.fg_normal
theme.titlebar_bg = theme.bg_normal

theme.titlebar_close_button_normal = theme_dir .. "/xresources/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme_dir .. "/xresources/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme_dir .. "/xresources/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme_dir .. "/xresources/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme_dir .. "/xresources/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme_dir .. "/xresources/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme_dir .. "/xresources/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme_dir .. "/xresources/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme_dir .. "/xresources/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme_dir .. "/xresources/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme_dir .. "/xresources/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme_dir .. "/xresources/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme_dir .. "/xresources/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme_dir .. "/xresources/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme_dir .. "/xresources/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme_dir .. "/xresources/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme_dir .. "/xresources/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme_dir .. "/xresources/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme_dir .. "/xresources/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme_dir .. "/xresources/titlebar/maximized_focus_active.png"

theme.menu_height = 14
theme.menu_width = 200
theme.menu_bg_normal = theme.bg_normal .. "66"
theme.menu_fg_normal = theme.fg_normal
theme.menu_bg_focus = theme.bg_focus .. "66"
theme.menu_fg_focus = theme.fg_focus
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, theme.xrdb.color10, theme.xrdb.color0)

theme.arch_icon  = theme_dir .. "/xresources/arch_logo.png"

-- collision
theme.collision_resize_bg = xrdb.color8
theme.collision_resize_fg = xrdb.color2
theme.collision_focus_fg = xrdb.color2
theme.collision_focus_bg = xrdb.color8
theme.collision_focus_bg_center = xrdb.color1

--theme = theme_assets.recolor_titlebar_normal(theme, theme.fg_normal)
--theme = theme_assets.recolor_titlebar_focus(theme, theme.fg_normal)
theme = theme_assets.recolor_layout(theme, theme.fg_normal)

naughty.config.defaults = {
    timeout = 5,
    text = "",
    screen = nil,
    ontop = true,
    margin = dpi(10),
    border_width = dpi(2),
    position = "top_right",
    bg = xrdb.color7,
    fg = xrdb.color8,
}

naughty.config.presets = {
    low = {
        bg = xrdb.color7,
        fg = xrdb.color8,
        timeout = 5,
    },
    normal = {
        bg = xrdb.color7,
        fg = xrdb.color8,
        timeout = 5,
    },
    critical = {
        bg = xrdb.color1,
        fg = xrdb.color0,
        timeout = 0,
    }
}

theme.notification_shape = function(cr, w, h)
    shape.infobubble(cr, w, h, 10, 5, w/2 + 20)
end

theme.wallpaper = function(s)
    return theme_assets.my_wp(theme.bg_normal, theme.xrdb.color12, s)
end

return theme
