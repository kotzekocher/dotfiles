local awful         = require "awful"
local beautiful     = require "beautiful"
local gears         = require "gears"
local naughty       = require "naughty"
local wibox         = require "wibox"
local debug         = require "gears.debug"
local wdgets        = require "kk.widgets"

awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.corner.nw,
}

-- applications

terminal = "urxvt"
lock_command = "lock.sh"

beautiful.wallpapers = {
    "/home/chris/img/wall.png",
}

clock = wibox.widget.textclock(" %a %d %b, %H:%M ")

application_menu = {
    {"terminal", "urxvt", functions.get_icon("terminal"), show_in_launcher = true },
    {"chrome", "google-chrome-stable", functions.get_icon("google-chrome-stable") },
    {"chromium", "chromium", functions.get_icon("chromium"), show_in_launcher = true },
    {"pcmanfm", "pcmanfm", functions.get_icon("file-manager"), show_in_launcher = true },
    {"gedit", "gedit", functions.get_icon("gedit") },
    {"-----------------", "nil" },
    {"thunderbird", "thunderbird", functions.get_icon("thunderbird-old"), show_in_launcher = true },
    {"spotify", "spotify.sh", functions.get_icon("spotify"), show_in_launcher = true },
    {"deadbeef", "deadbeef", functions.get_icon("deadbeef"), show_in_launcher = true },
    {"vlc", "vlc", functions.get_icon("vlc"), show_in_launcher = true },
    {"pavucontrol", "pavucontrol", functions.get_icon("audio-equalizer"), show_in_launcher = true },
    {"steam", "steam", functions.get_icon("steam"), show_in_launcher = true },
    { "zeug",
        {
           {"chrome-i", "google-chrome-stable --incognito", functions.get_icon("google-chrome-unstable")},
           {"ff-i", "firefox-nightly --private-window", functions.get_icon("firefox") },
        },
    },
    {"-----------------", "nil" },
    {"lock screen", lock_command, functions.get_icon("lock") },
    {"off", "my_reboot.sh" },
}

build_launcher_bar = function(menu)
    launcher_bar = { layout = wibox.layout.fixed.horizontal }
    for k,v in pairs(menu) do
        if v.show_in_launcher == true then
            launcher_bar[#launcher_bar+1] = awful.widget.launcher({ command = v[2], image = v[3] })
        end
    end
    return launcher_bar
end

mainmenu = awful.menu({ items = application_menu})
menulauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mainmenu })

lo = awful.layout.layouts
screen_layout = {
    tags = {
        { names =  { " www ", " code ", " code ", " code ", " mail ", " media " },
          layout = {  lo[2], lo[2], lo[2], lo[2], lo[2], lo[2] },
        },
    }
}

awful.screen.connect_for_each_screen(function(s)
    awful.tag(screen_layout.tags[s.index].names, s, screen_layout.tags[s.index].layout)
    functions.set_wallpaper(s)
    s.bar = awful.wibar({ position = "top", screen = s, height = 20 })
    s.taglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons, { spacing = -4 })
    s.tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons, { spacing = -4 })
    s.mypromptbox = awful.widget.prompt()
    s.lobox = awful.widget.layoutbox(s)
    s.bar:setup{
        layout = wibox.layout.align.horizontal,
        first = "left",
        third = "right",
        {
            layout = wibox.layout.fixed.horizontal,
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color12,
                {
                    widget = wibox.container.margin,
                    top = 1,
                    bottom = 1,
                    left = 7,
                    right = 7,
                    menulauncher,
                },
            },
            s.mypromptbox,
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color12,
                {
                    widget = wibox.container.margin,
                    top = 1,
                    bottom = 1,
                    left = 7,
                    right = 7,
                    build_launcher_bar(application_menu),
                }
            },
            -- widgets
            s.index == 1 and
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color7,
                fg = beautiful.xrdb.color0,
                {
                    widget = wibox.container.margin,
                    left = 5,
                    right = 5,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        widgets.cpu_widget(),
                        widgets.mem_widget(),
                    },
                },
            },
            s.index == 1 and
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color7,
                fg = beautiful.xrdb.color0,
                {
                    widget = wibox.container.margin,
                    left = 5,
                    right = 5,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        widgets.net_widget(),
                        widgets.bat_widget("BAT1","ACAD"),
                    },
                },
            },
            s.index == 1 and
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color7,
                fg = beautiful.xrdb.color0,
                {
                    widget = wibox.container.margin,
                    left = 5,
                    right = 5,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        widgets.weather_widget(),
                        widgets.pkg_widget(),
                    },
                },
            },
            -- widgets end
            {
                widget = wibox.container.constraint,
                width = 100,
                s.tasklist,
            },
        },
        nil,
        {
            layout = wibox.layout.fixed.horizontal,
            s.taglist,
            {
                widget = wibox.container.background,
                shape = beautiful.taglist_shape_function,
                bg = beautiful.xrdb.color12,
                fg = beautiful.xrdb.color7,
                {
                    widget = wibox.container.margin,
                    top = 2,
                    bottom = 2,
                    left = 5,
                    right = 7,
                    clock,
                }
            },
            s.index == 1 and {
                widget = wibox.container.background,
                shape = beautiful.taglist_shape_function,
                bg = beautiful.bg_systray,
                {
                    widget = wibox.container.margin,
                    top = 1,
                    bottom = 1,
                    left = 7,
                    right = 7,
                    wibox.widget.systray(),
                },
            },
            {
                widget = wibox.container.background,
                shape = beautiful.taglist_shape_function,
                bg = beautiful.xrdb.color12,
                {
                    widget = wibox.container.margin,
                    top = 2,
                    bottom = 2,
                    left = 5,
                    right = 7,
                    s.lobox,
                }
            }
        }
    }
end)

custom_rules = {
    { rule = { class = "Google-chrome" }, properties = { tag = screen[1].tags[1], switchtotag = true } },
    { rule = { class = "Chromium" }, properties = { tag = screen[1].tags[1], switchtotag = true } },
    { rule = { class = "Thunderbird" }, properties = { tag = screen[1].tags[5] } },
    { rule = { class = "Deadbeef" }, properties = { tag = screen[1].tags[6] } },
    { rule = { class = "Pavucontrol" }, properties = { tag = screen[1].tags[6], floating = true } },
    { rule = { class = "Spotify" }, properties = { tag = screen[1].tags[6] } },
    { rule = { class = "Steam" }, properties = { tag = screen[1].tags[5], floating = true } },
    { rule = { class = "TeamSpeak 3" }, properties = { tag = screen[1].tags[6], floating = true } },
    { rule = { class = "URxvt" }, properties = { border_width = 1 } },
}

awful.rules.rules = awful.util.table.join(awful.rules.rules, custom_rules)
