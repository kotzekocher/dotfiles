local xresources   = require("beautiful").xresources
local xrdb         = xresources.get_current_theme()
local dpi          = xresources.apply_dpi
local shape        = require("gears.shape")
local theme_assets = dofile(theme_dir .. "/xr/assets.lua")
local naughty      = require("naughty")
local functions      = require("kk.functions")

theme = {}

theme.font          = "Fira Mono for Powerline bold 10"
theme.xrdb = xrdb

theme.wallpapers = {}

theme.bg_normal     = xrdb.background
theme.bg_focus      = theme.bg_normal
theme.bg_urgent     = xrdb.color1
theme.bg_minimize   = theme.bg_normal
theme.bg_systray    = xrdb.color3

theme.fg_normal     = xrdb.foreground
theme.fg_focus      = xrdb.color5
theme.fg_urgent     = xrdb.color1
theme.fg_minimize   = xrdb.color7

theme.useless_gap   = dpi(1)
theme.border_width  = 1
theme.floating_border_width  = 1
theme.border_normal = xrdb.color0
theme.border_focused = xrdb.color5
theme.border_marked = xrdb.color5

theme.taglist_fg_focus = xrdb.color0
theme.taglist_bg_focus = xrdb.color11
theme.taglist_fg_occupied = xrdb.color0
theme.taglist_bg_occupied = xrdb.color3
theme.taglist_fg_empty = xrdb.color0
theme.taglist_bg_empty = xrdb.color5
theme.taglist_fg_urgent = xrdb.color0
theme.taglist_bg_urgent = xrdb.color1

theme.taglist_shape_function = function(cr,w,h)
  shape.powerline(cr,w,h,-2)
  --shape.partially_rounded_rect (cr, w, h, false, false, true, true, 4)
  --shape.rounded_rect(cr,w,h,4)
  --shape.parallelogram(cr, w, h, w - 3)
end
theme.taglist_shape = theme.taglist_shape_function

theme.tasklist_fg_normal = xrdb.color0
theme.tasklist_bg_normal = xrdb.color5
theme.tasklist_fg_focus = xrdb.color0
theme.tasklist_bg_focus = xrdb.color2
theme.tasklist_fg_urgent = xrdb.color8
theme.tasklist_bg_urgent = xrdb.color1
theme.tasklist_disable_icon = true
theme.tasklist_align = "left"

theme.tasklist_shape_function = function(cr,w,h)
  shape.powerline(cr,w,h,-2)
  --shape.parallelogram(cr, w, h, w - 3)
end
theme.tasklist_shape = theme.tasklist_shape_function

theme.end_shape = function(cr,w,h)
  shape.rectangular_tag(cr,w,h,2)
end

theme.end_shape2 = function(cr,w,h)
  shape.transform(shape.rectangular_tag) : rotate_at(35,35,math.pi/2) (cr,w,h,-2)
end

theme.widget_fg_normal = xrdb.color15
theme.widget_fg_green = xrdb.color2
theme.widget_fg_yellow = xrdb.color3
theme.widget_fg_orange = xrdb.color9
theme.widget_fg_red = xrdb.color1
theme.widget_fg_blue = xrdb.color12

theme.widget_bg_normal = xrdb.color2
theme.widget_bg_1 = xrdb.color5
theme.widget_bg_2 = xrdb.color3
theme.widget_bg_3 = xrdb.color2

theme.widget_icon_bat0 = ""
theme.widget_icon_bat1 = ""
theme.widget_icon_bat2 = ""
theme.widget_icon_bat3 = ""
theme.widget_icon_bat4 = ""
theme.widget_icon_charge = ""
theme.widget_icon_pkg = ""
theme.widget_icon_ram = ""
theme.widget_icon_cpu = ""
theme.widget_icon_net = ""
theme.widget_icon_wifi = ""
theme.widget_icon_ether = ""

theme.layout_fairh = theme_dir .. "/xr/layouts/fairhw.png"
theme.layout_fairv = theme_dir .. "/xr/layouts/fairvw.png"
theme.layout_floating  = theme_dir .. "/xr/layouts/floatingw.png"
theme.layout_magnifier = theme_dir .. "/xr/layouts/magnifierw.png"
theme.layout_max = theme_dir .. "/xr/layouts/maxw.png"
theme.layout_fullscreen = theme_dir .. "/xr/layouts/fullscreenw.png"
theme.layout_tilebottom = theme_dir .. "/xr/layouts/tilebottomw.png"
theme.layout_tileleft   = theme_dir .. "/xr/layouts/tileleftw.png"
theme.layout_tile = theme_dir .. "/xr/layouts/tilew.png"
theme.layout_tiletop = theme_dir .. "/xr/layouts/tiletopw.png"
theme.layout_spiral  = theme_dir .. "/xr/layouts/spiralw.png"
theme.layout_dwindle = theme_dir .. "/xr/layouts/dwindlew.png"
theme.layout_cornernw = theme_dir .. "/xr/layouts/cornernww.png"
theme.layout_cornerne = theme_dir .. "/xr/layouts/cornernew.png"
theme.layout_cornersw = theme_dir .. "/xr/layouts/cornersww.png"
theme.layout_cornerse = theme_dir .. "/xr/layouts/cornersew.png"

theme.titlebar_close_button_normal = theme_dir .. "/xr/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme_dir .. "/xr/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme_dir .. "/xr/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme_dir .. "/xr/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme_dir .. "/xr/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme_dir .. "/xr/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme_dir .. "/xr/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme_dir .. "/xr/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme_dir .. "/xr/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme_dir .. "/xr/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme_dir .. "/xr/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme_dir .. "/xr/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme_dir .. "/xr/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme_dir .. "/xr/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme_dir .. "/xr/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme_dir .. "/xr/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme_dir .. "/xr/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme_dir .. "/xr/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme_dir .. "/xr/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme_dir .. "/xr/titlebar/maximized_focus_active.png"

theme.menu_height = 25
theme.menu_width = 200
theme.menu_bg_normal = theme.bg_normal .. "66"
theme.menu_fg_normal = theme.fg_normal
theme.menu_bg_focus = theme.bg_focus .. "66"
theme.menu_fg_focus = theme.fg_focus
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, theme.xrdb.color10, theme.xrdb.color0)

theme.arch_icon  = theme_dir .. "/xr/arch_logo.png"

-- collision
theme.collision_resize_bg = xrdb.color8
theme.collision_resize_fg = xrdb.color2
theme.collision_focus_fg = xrdb.color1
theme.collision_focus_bg = xrdb.color0
theme.collision_focus_bg_center = xrdb.color4

theme = theme_assets.recolor_titlebar_normal(theme, theme.fg_normal)
theme = theme_assets.recolor_titlebar_focus(theme, theme.fg_normal)
theme = theme_assets.recolor_layout(theme, xrdb.color0)

naughty.config.defaults = {
  timeout = 5,
  text = "",
  screen = nil,
  ontop = true,
  margin = 10,
  border_width = dpi(2),
  position = "top_right",
  bg = xrdb.bg_normal,
  fg = xrdb.fg_normal,
  border_color = xrdb.color5,
}

naughty.config.presets = {
  low = {
    bg = xrdb.bg_normal,
    fg = xrdb.fg_normal,
    timeout = 5,
  },
  normal = {
    bg = xrdb.bg_normal,
    fg = xrdb.fg_normal,
    timeout = 5,
  },
  critical = {
    bg = xrdb.color1,
    fg = xrdb.color0,
    timeout = 0,
    border_color = xrdb.color15,
    border_width = 3,
  }
}

return theme
