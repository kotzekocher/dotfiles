local awful         = require "awful"
local beautiful     = require "beautiful"
local cairo         = require "lgi".cairo
local gears         = require "gears"
local gtable        = require "gears.table"
local naughty       = require "naughty"
local wibox         = require "wibox"
local debug         = require "gears.debug"
local widgets       = require "kk.widgets"
local functions     = require "kk.functions"
local lain          = require "lain"

awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.corner.nw,
    lain.layout.centerwork,
    lain.layout.termfair.center,
}

lain.layout.termfair.center.nmaster = 3
lain.layout.termfair.center.ncol    = 1

-- applications

terminal = "urxvtc"
lock_command = "lock.sh"

beautiful.wallpapers = {
    "/home/cdr/Bilder/wp/IMG_39993.png",
    "/home/cdr/Bilder/wp/IMG_39993.png",
    "/home/cdr/Bilder/wp/IMG_39993.png",
--    beautiful.xrdb.color0_light,
--    beautiful.xrdb.color0_light,
--    beautiful.xrdb.color0_light,
}

clock = wibox.widget.textclock(" [<span color=\"".. theme.taglist_fg_focus .. "\">%H:%M</span>] [<span color=\"".. theme.taglist_fg_focus .. "\">%a %d %b</span>] ")

dws = screen:count() > 1 and 2 or 1

desktop_clock = wibox.widget.textclock("<span font=\"DejaVu Sans Condensed Bold 60\" color=\"".. theme.taglist_fg_focus .. "\">%H:%M</span>")
desktop_date = wibox.widget.textclock("<span font=\"DejaVu Sans Condensed Bold 20\" color=\"".. theme.taglist_fg_focus .. "\"> %a %d %b </span>")


application_menu = {
    {"terminal", terminal, functions.get_icon("terminal"), show_in_launcher = true },
    {"firefox-nightly", "firefox-nightly", functions.get_icon("firefox-nightly"), show_in_launcher = true },
    {"chromium", "chromium", functions.get_icon("chromium"), show_in_launcher = true },
    {"meld", "meld", functions.get_icon("meld") },
    {"gedit", "gedit", functions.get_icon("gedit"), show_in_launcher = true },
    {"emacs", "emacs", functions.get_icon("emacs"), show_in_launcher = true },
    {"pcmanfm", "pcmanfm", functions.get_icon("file-manager"), show_in_launcher = true },
    {"wireshark", "wireshark-gtk", functions.get_icon("wireshark"), show_in_launcher = true },
    {"virtualbox", "VirtualBox", functions.get_icon("virtualbox"), show_in_launcher = true },
    {"virt-manager", "virt-manager", functions.get_icon("virt-manager"), show_in_launcher = true },
    {"-----------------", "nil" },
    {"hamster", "hamster", functions.get_icon("hamster"), show_in_launcher = true },
    {"thunderbird", "thunderbird", functions.get_icon("thunderbird") },
    {"pidgin", "pidgin", functions.get_icon("pidgin"), show_in_launcher = true },
    {"-----------------", "nil" },
    {"spotify", "spotify.sh", "/usr/share/icons/Moka/48x48/web/spotify-linux-48x48.png", show_in_launcher = true},
    {"pavucontrol", "pavucontrol", functions.get_icon("audio-equalizer"), show_in_launcher = true },
    {"nvidia-settings", "gksudo nvidia-settings", functions.get_icon("nvidia-settings") },
    { "screenlayout",
        {
            {"arandr", "arandr"},
            {"3 display", "/home/cdr/.screenlayout/office_3mon.sh"},
            {"1 display", "/home/cdr/.screenlayout/layout_1mon.sh"},
        },
    },
    { "zeug",
        {
            {"chrome", "google-chrome-stable", functions.get_icon("google-chrome-stable") },
            {"chrome-i", "google-chrome-stable --incognito", functions.get_icon("google-chrome-unstable")},
            {"ff-i", "firefox-nightly --private-window", functions.get_icon("firefox") },
        },
    },
    {"-----------------", "nil" },
    {"lock screen", lock_command, functions.get_icon("lock") },
    {"off", "my_reboot.sh" },
}
--[[
build_launcher_bar = function(menu)
    launcher_bar = { layout = wibox.layout.fixed.horizontal }
    for k,v in pairs(menu) do
        if v.show_in_launcher == true then
            launcher_bar[#launcher_bar+1] = awful.widget.launcher({ command = v[2], image = v[3] })
        end
    end
    return launcher_bar
end
]]--
mainmenu = awful.menu({ items = application_menu })
menulauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mainmenu })

lo = awful.layout.layouts
screen_layout = {
    tags = {
        { names =  { " www ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ",  " 9 " },
          layout = { lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2] },
        },
        { names =  { " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " media ", " notes ", " mail " },
          layout = { lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2] },
        },
        { names =  { " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 " },
          layout = { lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2], lo[2] },
        },
    }
}

local calendar_popup = awful.widget.calendar_popup.month({position = "tr"})
clock:connect_signal("button::press", function()
    calendar_popup.screen = awful.screen.focused()
    calendar_popup:toggle()
end)

awful.screen.connect_for_each_screen(function(s)
    awful.tag(screen_layout.tags[s.index].names, s, screen_layout.tags[s.index].layout)
    functions.set_wallpaper(s)
    s.bar = awful.wibar({ position = "top", screen = s, height = 20, bg = theme.xrdb.color4 .. "55" })
    s.taglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons, { spacing = -4 })
    s.tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons, { spacing = -4 })
    s.mypromptbox = awful.widget.prompt()
    s.lobox = awful.widget.layoutbox(s)
    --[[
    if s.index == dws then
        if screen:count() > 1 then
            s.padding = { right=s.padding.right+300 }
        end
        wa = s.workarea
        desktopbox = wibox({type = "desktop", x = wa.width - 300, y = wa.y, width = 300, height = wa.height, visible = true, bg = "#00000000" })
        desktopbox:setup{
            layout = wibox.layout.fixed.vertical,
            functions.sep_top_box(300,30,10,"#00000000","#ffffff"),
            {
                layout = wibox.layout.align.horizontal,
                expand = "outside",
                nil,
                {
                    layout = wibox.layout.fixed.vertical,
                    desktop_clock,
                    desktop_date,
                },
                nil,
            },
            functions.sep_bot_box(300,30,10,"#00000000","#ffffff"),
            functions.sep_top_box(300,30,10,"#00000000","#ffffff"),
            widgets.cpu_graph_widget(),
            functions.sep_bot_box(300,30,10,"#00000000","#ffffff"),
            functions.sep_top_box(300,30,10,"#00000000","#ffffff"),
            widgets.net_widget_graph(),
            functions.sep_bot_box(300,30,10,"#00000000","#ffffff"),
            functions.sep_top_box(300,30,10,"#00000000","#ffffff"),
            functions.sep_bot_box(300,30,10,"#00000000","#ffffff"),
        }
    end
    ]]--
    s.bar:setup{
        layout = wibox.layout.align.horizontal,
        first = "left",
        third = "right",
        {
            layout = wibox.layout.fixed.horizontal,
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color0,
                {
                    widget = wibox.container.margin,
                    top = 1,
                    bottom = 1,
                    left = 7,
                    right = 7,
                    menulauncher,
                },
            },
            s.mypromptbox,
            -- widgets
            s.index == 1 and
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color0,
                fg = beautiful.xrdb.color15,
                {
                    widget = wibox.container.margin,
                    left = 5,
                    right = 5,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        widgets.cpu_widget(),
                        widgets.mem_widget(),
                    },
                },
            },
            s.index == 1 and
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color0,
                fg = beautiful.xrdb.color15,
                {
                    widget = wibox.container.margin,
                    left = 5,
                    right = 5,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        widgets.net_widget(),
                        widgets.bat_widget("BAT0","AC0"),
                    },
                },
            },
            s.index == 1 and
            {
                widget = wibox.container.background,
                shape = beautiful.tasklist_shape_function,
                bg = beautiful.xrdb.color0,
                fg = beautiful.xrdb.color15,
                {
                    widget = wibox.container.margin,
                    left = 5,
                    right = 5,
                    {
                        layout = wibox.layout.fixed.horizontal,
                        widgets.weather_widget(),
                        widgets.pkg_widget(),
                        --widgets.spotify_widget(),
                    },
                },
            },
            -- widgets end
            {
                widget = wibox.container.constraint,
                width = 800,
                s.tasklist,
            },
        },
        nil,
        {
            layout = wibox.layout.fixed.horizontal,
            s.taglist,
            {
                widget = wibox.container.background,
                shape = beautiful.taglist_shape_function,
                bg = beautiful.xrdb.color0,
                fg = beautiful.xrdb.color7,
                {
                    widget = wibox.container.margin,
                    top = 2,
                    bottom = 2,
                    left = 5,
                    right = 7,
                    clock,
                }
            },
            s.index == 1 and {
                widget = wibox.container.background,
                shape = beautiful.taglist_shape_function,
                bg = beautiful.bg_systray,
                {
                    widget = wibox.container.margin,
                    top = 1,
                    bottom = 1,
                    left = 7,
                    right = 7,
                    wibox.widget.systray(),
                },
            },
            {
                widget = wibox.container.background,
                shape = beautiful.taglist_shape_function,
                bg = beautiful.xrdb.color0,
                {
                    widget = wibox.container.margin,
                    top = 2,
                    bottom = 2,
                    left = 5,
                    right = 7,
                    s.lobox,
                }
            }
        }
    }
end)
s1 = 1
s2 = screen:count() > 1 and 2 or 1
s3 = screen:count() > 2 and 3 or 1
custom_rules = {
    { rule = { class = "Firefox" }, properties = { tag = screen[s1].tags[1] } },
    { rule = { role = "Dialog" }, properties = { floating = true } },
    { rule = { class = "Hamster" }, properties = { tag = screen[s2].tags[8] }, callback = function(c) awful.client.setslave(c) end },
    { rule = { class = "Thunderbird" }, properties = { tag = screen[s2].tags[9] } },
    { rule = { class = "Chromium" }, properties = { screen = screen[s3] } },
    { rule = { class = "Spotify" }, properties = { tag = screen[s2].tags[7], floating = false, callback = function(c) awful.client.setmaster(c) end } },
    { rule = { class = "Emacs" }, properties = { tag = screen[s2].tags[8] } },
    { rule = { class = "Pidgin", role = "buddy_list" }, properties = { tag = screen[s1].tags[9], x = 10, y = 35, width = 250, height = 950, floating = true } },
    { rule = { class = "Pidgin", role = "conversation" }, properties = { tag = screen[s1].tags[9], x = 270, y = 70, width = 1000, height = 800, floating = true} },
    { rule = { class = "URxvt" }, properties = { border_width = 1 } },
}

awful.rules.rules = awful.util.table.join(awful.rules.rules, custom_rules)
