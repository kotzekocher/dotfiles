local awful         = require "awful"
local beautiful     = require "beautiful"
local gears         = require "gears"
local naughty       = require "naughty"
local wibox         = require "wibox"
local debug         = require "gears.debug"
local widgets       = require "kk.widgets"

awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.corner.nw,
}

-- applications

terminal = "urxvt"
lock_command = "lock.sh"

s1 = 1
s2 = screen:count() > 1 and 2 or 1

beautiful.wallpapers = {
--    "/home/chris/Bilder/img/wallhaven-490733_2560x1440.png",
--    "/home/chris/Bilder/img/wallhaven-490733_2560x1440.png",
--    "/home/chris/Bilder/img/wallhaven-376314_2560x1440.png",
--    "/home/chris/Bilder/img/wallhaven-376314_2560x1440.png",
    "/home/chris/Bilder/img/wallhaven-124509.jpg",
    "/home/chris/Bilder/img/wallhaven-124509_1920x1080.png",
}

clock = wibox.widget.textclock(" [<span color=\"".. theme.taglist_fg_focus .. "\">%H:%M</span>] [<span color=\"".. theme.taglist_fg_focus .. "\">%a %d %b</span>] ")

application_menu = {
    {"terminal", "urxvt", functions.get_icon("terminal"), show_in_launcher = true },
    {"firefox-nightly", "firefox-nightly", functions.get_icon("firefox-nightly"), show_in_launcher = true },
    {"chrome", "google-chrome-stable", functions.get_icon("google-chrome-stable"), show_in_launcher = true },
    {"chromium", "chromium", functions.get_icon("chromium") },
    {"pcmanfm", "pcmanfm", functions.get_icon("file-manager"), show_in_launcher = true },
    {"gedit", "gedit", functions.get_icon("gedit") },
    {"kdeconnect", "indicator-kdeconnect", functions.get_icon("kdeconnect") },
    {"-----------------", "nil" },
    {"thunderbird", "thunderbird", functions.get_icon("thunderbird-old"), show_in_launcher = true },
    {"evolution", "evolution", functions.get_icon("evolution"), show_in_launcher = true },
    {"spotify", "spotify.sh", functions.get_icon("spotify"), show_in_launcher = true },
    {"deadbeef", "deadbeef", functions.get_icon("deadbeef"), show_in_launcher = true },
    {"vlc", "vlc", functions.get_icon("vlc"), show_in_launcher = true },
    {"pavucontrol", "pavucontrol", functions.get_icon("audio-equalizer"), show_in_launcher = true },
    {"steam", "steam", functions.get_icon("steam"), show_in_launcher = true },
    {"teamspeak 3", "teamspeak3", functions.get_icon("teamspeak3"), show_in_launcher = true },
    {"playonlinux", "playonlinux", functions.get_icon("playonlinux"), show_in_launcher = true },
    { "zeug",
        {
           {"chrome-i", "google-chrome-stable --incognito", functions.get_icon("google-chrome-unstable")},
           {"ff-i", "firefox-nightly --private-window", functions.get_icon("firefox") },
        },
    },
    {"-----------------", "nil" },
    {"lock screen", lock_command, functions.get_icon("lock") },
    {"off", "my_reboot.sh" },
}

build_launcher_bar = function(menu)
    launcher_bar = { layout = wibox.layout.fixed.horizontal }
    for k,v in pairs(menu) do
        if v.show_in_launcher == true then
            launcher_bar[#launcher_bar+1] = awful.widget.launcher({ command = v[2], image = v[3] })
        end
    end
    return launcher_bar
end

mainmenu = awful.menu({ items = application_menu})
menulauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mainmenu })

lo = awful.layout.layouts
screen_layout = {
    tags = {
        { names =  { "ff", "chrome", "code", "code", "gaming"},
          layout = { lo[2], lo[2], lo[2], lo[2], lo[1]},
        },
        { names =  { "browse", "mail", "notes", "media"},
          layout = { lo[2], lo[2], lo[2], lo[1],},
        },
    }
}


awful.screen.connect_for_each_screen(function(s)
    awful.tag(screen_layout.tags[s.index].names, s, screen_layout.tags[s.index].layout)
    functions.set_wallpaper(s)
    s.bar = awful.wibar({ position = "top", screen = s, height = 16 })
    s.taglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons, { spacing = 0 })
    s.tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons, { spacing = 0 })
    s.mypromptbox = awful.widget.prompt()
    s.lobox = awful.widget.layoutbox(s)
    s.bar:setup{
        layout = wibox.layout.align.horizontal,
        first = "left",
        third = "right",
        {
            layout = wibox.layout.fixed.horizontal,
            menulauncher,
            s.tasklist,
        },
        s.mypromptbox,
        {
            layout = wibox.layout.fixed.horizontal,
            s.taglist,
            clock,
            wibox.widget.systray(),
            s.lobox,
        }
    }
    --[[
    if s.index == s2 then
        wa = s.workarea
        desktopbox = wibox({type = "desktop", x = wa.width - 620 + 2560, y = s.geometry.height - 300, width = 1000, height = 300, visible = true, bg = "#00000000" })
        desktopbox:setup{
            layout = wibox.layout.stack,
            {
                widget = wibox.widget.imagebox,
                image = functions.get_grid(620,300,20,"#00000000",beautiful.xrdb.color1 .. "44"),
                resize = false,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                widgets.cpu_graph_widget(),
                widgets.mem_graph_widget(),
                widgets.net_widget_graph(),
            }
        }
    end
    ]]--
end)

custom_rules = {
    { rule = { class = "Firefox" }, properties = { tag = screen[s1].tags[1], switchtotag = true } },
    { rule = { class = "Google-chrome" }, properties = { tag = screen[s1].tags[2], switchtotag = true } },
    { rule = { class = "Thunderbird" }, properties = { tag = screen[s2].tags[2] } },
    { rule = { class = "Deadbeef" }, properties = { tag = screen[s2].tags[4] } },
    { rule = { class = "Pavucontrol" }, properties = { tag = screen[s2].tags[4] } },
    { rule = { class = "Spotify" }, properties = { tag = screen[s2].tags[4] } },
    { rule = { class = "Steam" }, properties = { tag = screen[s1].tags[5] } },
    { rule = { name = "PlayOnLinux" }, properties = { tag = screen[s1].tags[5] } },
    { rule = { name = "LolClient.exe" }, properties = { tag = screen[s1].tags[5] } },
    { rule = { name = "PathOfExile.exe" }, properties = { tag = screen[s1].tags[5], floating = true } },
    { rule = { class = "StardewValley.bin.x86_64" }, properties = { tag = screen[s1].tags[5], floating = true } },
    { rule = { class = "TeamSpeak 3" }, properties = { tag = screen[s2].tags[4] } },
    { rule = { class = "URxvt" }, properties = { border_width = 1 } },
}

awful.rules.rules = awful.util.table.join(awful.rules.rules, custom_rules)
