local awful         = require "awful"
local beautiful     = require "beautiful"
local gears         = require "gears"
local naughty       = require "naughty"
local wibox         = require "wibox"
local debug         = require "gears.debug"
local widgets       = require "kk.widgets_new"
local machines      = require "kk.machines"

awful.layout.layouts = {
  awful.layout.suit.floating,
  awful.layout.suit.tile,
  awful.layout.suit.tile.bottom,
  awful.layout.suit.fair,
  awful.layout.suit.fair.horizontal,
  awful.layout.suit.corner.nw,
}

beautiful.wallpapers = machines.get_wallpaper()

clock = wibox.widget.textclock("<span> %a %d %b %H:%M</span> ")

application_menu = machines.get_menu()
mainmenu = awful.menu({ items = application_menu})
menulauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mainmenu })

awful.screen.connect_for_each_screen(function(s)
  machines.get_tags(s)
  functions.set_wallpaper(s)
  s.bar = awful.wibar({ position = "top", screen = s, height = 16 })
  s.taglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons, { spacing = 0 })
  s.tasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons, { spacing = 0} )
  s.lobox = awful.widget.layoutbox(s)
  s.bar:setup{
    layout = wibox.layout.align.horizontal,
    first = "left",
    second = "left",
    third = "right",
    {
      layout = wibox.layout.fixed.horizontal,
      {
	widget = wibox.container.background,
	bg = beautiful.widget_bg_1,
	fg = beautiful.widget_bg_2,
	shape = beautiful.end_shape2,
	{
	  widget = wibox.container.margin,
	  left = 5,
	  right = 5,
	  menulauncher,
	},
      },
      {
	widget = wibox.container.background,
	bg = beautiful.widget_bg_2,
	fg = beautiful.xrdb.color0,
	shape = beautiful.taglist_shape_function,
	{
	  widget = wibox.container.margin,
	  top = 1,
	  bottom = 1,
	  left = 7,
	  right = 7,
	  wibox.widget.systray(),
	},
      },
      {
	widget = wibox.container.background,
	bg = beautiful.widget_bg_1,
	fg = beautiful.xrdb.color0,
	shape = beautiful.taglist_shape_function,
	widgets.widget_bar,
      },
    },
    {
      widget = wibox.container.constraint,
      width = 500,
      s.tasklist,
    },
    {
      layout = wibox.layout.fixed.horizontal,
      s.taglist,
      {
	widget = wibox.container.background,
	bg = beautiful.widget_bg_1,
	fg = beautiful.xrdb.color0,
	shape = beautiful.taglist_shape_function,
	clock,
      },
      {
	widget = wibox.container.background,
	bg = beautiful.widget_bg_2,
	fg = beautiful.xrdb.color0,
	shape = beautiful.taglist_shape_function,
	widgets.worldclock,
      },
      {
	widget = wibox.container.background,
	bg = beautiful.widget_bg_1,
	shape = beautiful.end_shape,
	{
	  widget = wibox.container.margin,
	  left = 4,
	  right = 4,
	  s.lobox,
	}
      }
    },
  }
end)

awful.rules.rules = awful.util.table.join(awful.rules.rules, machines.get_rules())
