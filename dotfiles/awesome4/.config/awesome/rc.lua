local gears         = require "gears"
local awful         = require "awful"
require "awful.autofocus"
local wibox         = require "wibox"
local naughty       = require "naughty"
local beautiful     = require "beautiful"
local lain          = require "lain"
local collision     = require "collision"

collision()

markup = lain.util.markup

theme = os.getenv("AWESOME_THEME")
config = os.getenv("AWESOME_CONFIG")
awesome_dir = os.getenv("HOME") .. "/code/repo/dotfiles/dotfiles/awesome4/.config/awesome/"
theme_dir = awesome_dir .. "themes"

beautiful.init(theme_dir .. "/" .. theme .. "/theme.lua")

functions = require "kk.functions"
errors    = require "kk.errors"
key       = require "kk.keys"
signals   = require "kk.signals"
rules     = require "kk.rules"

require("themes." .. config)

