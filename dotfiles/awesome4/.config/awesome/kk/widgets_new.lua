local beautiful = require "beautiful"
local lain      = require "lain"
local wibox     = require "wibox"
local awful     = require "awful"
local gears     = require "gears"
local naughty   = require "naughty"
local dbus      = dbus
markup = lain.util.markup

widgets = {}

monitors = {}
monitors.wlan_essid = ""
monitors.wlan_mac = ""
monitors.wlan_bitrate = ""
monitors.wlan_ip = ""
monitors.wlan_int = "wlp1s0"
monitors.wlan_net_connected = false
monitors.wlan_signal_level = 0
monitors.wlan_exists = true
monitors.bat_status = ""

widgets.widget_bar = wibox.widget {
    {
        widget = wibox.container.margin,
        left = 5,
    },
    {
        id = "cpu_widget",
        widget = wibox.widget.textbox,
    },
    {
        widget = wibox.container.margin,
        left = 5,
    },
    {
        id = "bat_widget",
        widget = wibox.widget.textbox,
    },
    {
        widget = wibox.container.margin,
        left = 5,
    },
    {
        id = "net_widget",
        text = beautiful.widget_icon_wifi,
        widget = wibox.widget.textbox,
    },
    {
        widget = wibox.container.margin,
        left = 5,
    },
    layout = wibox.layout.fixed.horizontal,
}

widgets.worldclock = wibox.widget {
    {
        id = "clock_berlin",
        widget = wibox.widget.textbox,
    },
    layout = wibox.layout.fixed.horizontal,
}

local cpu_widget = lain.widget.cpu({
    settings = function()
      usage = string.format("%03d", cpu_now.usage)
      --[[
      if cpu_now.usage > 90 then
	usage = markup(beautiful.widget_fg_red, usage)
      elseif cpu_now.usage > 50 then
	usage = markup(beautiful.widget_fg_orange, usage)
      else
	usage = markup(beautiful.widget_fg_green, usage)
      end
      ]]--
      usage = markup(beautiful.xrdb.color0, usage)
      widgets.widget_bar:get_children_by_id("cpu_widget")[1].markup = beautiful.widget_icon_cpu .. " " .. usage
    end
})

--local battery_notification
local function show_battery_status()
    awful.spawn.easy_async([[bash -c 'acpi']],
        function(stdout, _, _, _)
            battery_notification = naughty.notify{
                text =  stdout,
                position = "top_left",
                timeout = 3,
            }
        end
    )
end

worldclock_timer = gears.timer {
    timeout = 10,
    autostart = true,
    callback = function()
                awful.spawn.easy_async({"sh", "-c", "TZ=\"Europe/Berlin\" date +%H:%M"},
                function(out)
                    time = string.gsub(out,"\n","")
                    widgets.worldclock:get_children_by_id("clock_berlin")[1].markup = "<span> Berlin " .. time .. " </span>"
                end
            )
    end
}

awful.widget.watch("acpi", 10,
    function(widget, stdout, stderr, exitreason, exitcode)
        local batteryType
        local _, status, charge_str, time = string.match(stdout, '(.+): (%a+), (%d+)%%,? ?.*')
        local charge = tonumber(charge_str)
        if charge <= 10 then
            color = beautiful.widget_fg_red
            icon = beautiful.widget_icon_bat0
        elseif (charge > 10 and charge <= 25) then
            color = beautiful.widget_fg_orange
            icon = beautiful.widget_icon_bat1
        elseif (charge > 25 and charge <= 50) then
            color = beautiful.widget_fg_yellow
            icon = beautiful.widget_icon_bat2
        elseif (charge > 50 and charge <= 75) then
            color = beautiful.widget_fg_blue
            icon = beautiful.widget_icon_bat3
        elseif (charge > 75 and charge <= 100) then
            color = beautiful.widget_fg_blue
            icon = beautiful.widget_icon_bat4
        end
        if status ~= 'Discharging' then
            color = beautiful.widget_fg_blue
            icon = beautiful.widget_icon_charge
        end
	color = beautiful.xrdb.color0
        perc = markup(color, charge)
        icon = markup(color, icon)
        widgets.widget_bar:get_children_by_id("bat_widget")[1].markup = icon .. " " .. perc
    end
)

awful.widget.watch("awk 'NR==3 {printf \"%3.0f\" ,($3/70)*100}' /proc/net/wireless", 10,
    function(widget, stdout, stderr, exitreason, exitcode)
        if monitors.wlan_exists == true then
            monitors.wlan_signal_level = tonumber( stdout )
            if monitors.wlan_signal_level == nil then
                monitors.wlan_net_connected = false
            elseif monitors.wlan_signal_level <= 25 then
                monitors.wlan_net_connected = true
            elseif (monitors.wlan_signal_level > 25 and monitors.wlan_signal_level <= 50) then
                monitors.wlan_net_connected = true
            elseif (monitors.wlan_signal_level > 50 and monitors.wlan_signal_level <= 75) then
                monitors.wlan_net_connected = true
            else
                monitors.wlan_net_connected = true
            end
        else
            monitors.wlan_net_connected = false
        end
end)

local function wlan_check()
    awful.spawn.with_line_callback("iw dev "..monitors.wlan_int.." link", {
        stdout = function(line)
            monitors.wlan_mac = string.match(line, "Connected to ([0-f:]+)") or monitors.wlan_mac
            monitors.wlan_essid = string.match(line, "SSID: (.+)") or monitors.wlan_essid
            monitors.wlan_bitrate = string.match(line, "tx bitrate: (.+/s)") or monitors.wlan_bitrate
        end
    })

    awful.spawn.with_line_callback("ip addr show "..monitors.wlan_int, {
        stdout = function(line)
            monitors.wlan_ip = string.match(line, "inet (%d+%.%d+%.%d+%.%d+)") or monitors.wlan_ip
        end
    })
end

local function wlan_status()
    local msg = ""
    if monitors.wlan_net_connected then
        msg =
            "<span font_desc=\""..theme.font.."\">"..
            "┌["..monitors.wlan_int.."]\n"..
            "├ESSID:\t\t"..monitors.wlan_essid.."\n"..
            "├BSSID:\t\t"..monitors.wlan_mac.."\n"..
            "├IP:\t\t"..monitors.wlan_ip.."\n"..
            "├Signal:\t"..monitors.wlan_signal_level.."%\n"..
            "└Bit rate:\t"..monitors.wlan_bitrate.."</span>"
    else
        msg = "Wireless network is disconnected"
    end

    return msg
end

function net_notify_show()
    net_notification = naughty.notify({
        text = wlan_status(),
        position = "top_left",
        timeout = 0,
    })
end

wlan_check()

wlan_check_timer = gears.timer{
    timeout = 10,
    autostart = true,
    callback = function() wlan_check() end
}

widgets.widget_bar:get_children_by_id("net_widget")[1]:connect_signal("mouse::enter", function () net_notify_show() end)
widgets.widget_bar:get_children_by_id("net_widget")[1]:connect_signal("mouse::leave", function () naughty.destroy(net_notification) end)

widgets.widget_bar:get_children_by_id("bat_widget")[1]:connect_signal("mouse::enter", function () show_battery_status() end)
widgets.widget_bar:get_children_by_id("bat_widget")[1]:connect_signal("mouse::leave", function () naughty.destroy(battery_notification) end)

return widgets
