local awful = require "awful"
local wibox = require "wibox"

local hostname = os.getenv("HOSTNAME")
local hostname = "bender"


--terminal = "kitty --listen-on unix:/tmp/mykitty"
terminal = "alacritty"
lock_command = "lock.sh"

machines = {}

function machines.get_wallpaper()
  wp = {
    bender = {
      "/home/chris/img/wallpaper.jpg",
    }
  }
  return wp[hostname]
end

function machines.get_menu()
  menu = {
    bender = {
--      {"mail", "kitty --listen-on unix:/tmp/mykitty neomutt", functions.get_icon("email") },
      {"mail", "alacritty -e neomutt", functions.get_icon("email") },
      {"bluetooth", "blueman-manager", functions.get_icon("bluetooth") },
      {"spotify", "spotify.sh", functions.get_icon("spotify")  },
      {"deadbeef", "deadbeef", functions.get_icon("deadbeef")  },
      {"pavucontrol", "pavucontrol", functions.get_icon("audio-equalizer")  },
      {"steam", "steam", functions.get_icon("steam")  },
      {"syncthing-gtk", "syncthing-gtk", functions.get_icon("syncthing") },
      {"-----------------", "nil" },
      {"lock screen", lock_command, functions.get_icon("lock") },
      {"off", "my_reboot.sh" },
    }
  }
  return menu[hostname]
end

function machines.get_tags(s)
  tags = {
    bender = function(s)
      awful.tag.add("www",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s, selected = true })
      awful.tag.add("emacs",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
      awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
      awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
      awful.tag.add("mail",{ layout = awful.layout.suit.tile, gap = 10, screen = s})
      awful.tag.add("media",{ layout = awful.layout.suit.tile, gap = 10, screen = s})
    end,
    odin = function(s)
      if s.index == 1 then
	awful.tag.add("www",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s, selected = true })
	awful.tag.add("emacs",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("gaming",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
      elseif s.index == 2 then
	awful.tag.add("www",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s, selected = true })
	awful.tag.add("emacs",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("mail",{ layout = awful.layout.suit.tile, gap = 10, screen = s})
	awful.tag.add("media",{ layout = awful.layout.suit.tile, gap = 10, screen = s})
      end
    end,
    droppnix = function(s)
      if s.index == 1 then
	awful.tag.add("www",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s, selected = true })
	awful.tag.add("emacs",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
      elseif s.index == 2 then
	awful.tag.add("www",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s, selected = true })
	awful.tag.add("emacs",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("mail",{ layout = awful.layout.suit.tile, gap = 10, screen = s})
	awful.tag.add("media",{ layout = awful.layout.suit.tile, gap = 10, screen = s})
      elseif s.index == 3 then
	awful.tag.add("www",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s, selected = true })
	awful.tag.add("emacs",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
	awful.tag.add("term",{ layout = awful.layout.suit.tile, gap_single_client = false, screen = s})
      end
    end
  }
  tags[hostname](s)
end

function create_tag_if_not_there(name, c)
  if not awful.tag.find_by_name(nil,name) then
    t = awful.tag.add(name, { layout = awful.layout.suit.tile, gap_single_client = false, volatile = true, screen = s1 })
    c:move_to_tag(t)
  end
end

function machines.get_rules()
  s1 = 1
  s2 = screen:count() > 1 and 2 or 1
  s3 = screen:count() > 2 and 3 or 1
  rules = {
    bender = {
      { rule = { class = "Google-chrome" },
	callback = function(c) create_tag_if_not_there("google-chrome", c) end,
      },
      { rule = { class = "digikam" },
	callback = function(c) create_tag_if_not_there("digikam", c) end,
	properties = { tag = awful.tag.find_by_name(nil,"digikam") },
      },
      { rule = { class = "calibre" },
	callback = function(c) create_tag_if_not_there("calibre", c) end,
	properties = { tag = awful.tag.find_by_name(nil,"calibre") },
      },
      { rule_any = { class = {"Gimp-2.10", "Gimp"} },
	callback = function(c) create_tag_if_not_there("gimp", c) end,
	properties = { tag = awful.tag.find_by_name(nil,"gimp") },
      },
      { rule = { class = "Nightly" }, properties = { tag = awful.tag.find_by_name(nil, "www")} },
      { rule = { class = "Deadbeef" }, properties = { tag = awful.tag.find_by_name(nil ,"media") } },
      { rule = { class = "Pavucontrol" }, properties = { tag = awful.tag.find_by_name(nil ,"media"), floating = true } },
      { rule = { class = "Spotify" }, properties = { tag = awful.tag.find_by_name(nil, "media") } },
      { rule = { class = "Steam" },
	callback = function(c) create_tag_if_not_there("gaming", c) end,
	properties = { tag = awful.tag.find_by_name(nil,"gaming") },
      },
      { rule = { class = "TeamSpeak 3" },
	callback = function(c) create_tag_if_not_there("gaming", c) end,
	properties = { tag = awful.tag.find_by_name(nil,"gaming") },
      },
      { rule = { class = "Emacs" }, properties = { tag = awful.tag.find_by_name(nil, "emacs") },
	callback = function(c) awful.client.setmaster(c) end,
      },
    }
  }
  return rules[hostname]
end

function machines.get_statusbar(s)
  m = {
    bender = {
      layout = wibox.layout.fixed.horizontal, wibox.widget.textclock("<span> %a %d %b %H:%M</span> "),
      {},
    }
  }
  return m[hostname]
end

return machines
