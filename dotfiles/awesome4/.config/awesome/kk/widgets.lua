local beautiful = require "beautiful"
local lain      = require "lain"
local wibox     = require "wibox"
local awful     = require "awful"
local gears     = require "gears"
local dbus      = dbus
markup = lain.util.markup

widgets = {}

function update_widget(w, txt_icon, value)
    w:set_markup(" " .. txt_icon .. " " .. value .. " ")
end


function widgets.cpu_widget()
    cpu_widget = lain.widget.cpu({
        settings = function()
            usage = string.format("%03d", cpu_now.usage)
            if cpu_now.usage > 90 then
                usage = markup(beautiful.widget_fg_red, usage)
            elseif cpu_now.usage > 50 then
                usage = markup(beautiful.widget_fg_orange, usage)
            else
                usage = markup(beautiful.widget_fg_blue, usage)
            end
            update_widget(widget, beautiful.widget_icon_cpu, usage)
        end
    })
    return cpu_widget
end

function widgets.cpu_graph_widget()
    cpu_graph =  wibox.widget {
        widget = wibox.widget.graph,
        min_value = 0,
        max_value = 100,
        color = beautiful.xrdb.color4,
        background_color = "#00000000",
        step_width = 3,
        step_spacing = 1,
        forced_width = 300,
        forced_height = 100,
    }
    cpu_widget = lain.widget.cpu({
        settings = function()
            cpu_graph:add_value(cpu_now.usage)
        end
    })
    return cpu_graph
end


function widgets.mem_graph_widget()
    mem_graph =  wibox.widget {
        widget = wibox.widget.progressbar,
        min_value = 0,
        max_value = 100,
        color = beautiful.xrdb.color6,
        background_color = "#00000000",
        border_width = 0,
        border_color = beautiful.xrdb.color0,
        forced_width = 300,
        forced_height = 20,
    }
    mem_widget = lain.widget.mem({
        settings = function()
            mem_graph:set_value(mem_now.perc)
        end
    })
    mem_layout = wibox.widget{
        widget = wibox.container.rotate,
        direction = "east",
        mem_graph,
    }
    return mem_graph
end

function widgets.net_widget_graph()
    down_graph = wibox.widget {
        widget = wibox.widget.graph,
        min_value = 0,
        max_value = 100,
        color = beautiful.xrdb.color2,
        background_color = "#00000000",
        forced_width = 300,
        forced_height = 50,
        step_width = 3,
        step_spacing = 1,
    }
    up_graph = wibox.widget {
        widget = wibox.widget.graph,
        min_value = 0,
        max_value = 100,
        color = beautiful.xrdb.color5,
        background_color = "#00000000",
        forced_width = 300,
        forced_height = 50,
        step_width = 3,
        step_spacing = 1,
    }
    net_widget = lain.widget.net({
        settings = function()
            mbps_rcv = net_now.received /1024 * 8
            mbps_snd = net_now.sent / 1024 * 8
            down_graph:add_value(mbps_rcv)
            up_graph:add_value(mbps_snd)
        end
    })

    net_layout = {
        layout = wibox.layout.fixed.vertical,
        up_graph,
        {
            widget = wibox.container.mirror,
            reflection = { vertical = true },
            down_graph,
        },
    }
    return net_layout
end

function widgets.mem_widget()
    mem_widget = lain.widget.mem({
        settings = function()
            update_widget(widget, beautiful.widget_icon_ram, mem_now.used .. "/" .. mem_now.total)
        end
    })
    return mem_widget
end

function widgets.bat_widget(bat_name,ac_name)
    mybat = lain.widget.bat({
        battery = bat_name,
        ac = ac_name,
        --notify = "off",
        n_perc = {5,10},
        settings = function()
            if bat_now.ac_status == 0 then
                if bat_now.perc > 90 then
                    perc = markup(beautiful.widget_fg_blue, bat_now.perc)
                    icon = markup(beautiful.widget_fg_blue, beautiful.widget_icon_bat4)
                elseif bat_now.perc > 70 then
                    perc = markup(beautiful.widget_fg_blue, bat_now.perc)
                    icon = markup(beautiful.widget_fg_blue, beautiful.widget_icon_bat3)
                elseif bat_now.perc > 50 then
                    perc = markup(beautiful.widget_fg_orange, bat_now.perc)
                    icon = markup(beautiful.widget_fg_orange, beautiful.widget_icon_bat2)
                elseif bat_now.perc > 20 then
                    perc = markup(beautiful.widget_fg_red, bat_now.perc)
                    icon = markup(beautiful.widget_fg_red, beautiful.widget_icon_bat1)
                else
                    perc = markup(beautiful.widget_fg_red, bat_now.perc)
                    icon = markup(beautiful.widget_fg_red, beautiful.widget_icon_bat0)
                end
            else
                perc = markup(beautiful.widget_fg_blue, bat_now.perc)
                icon = markup(beautiful.widget_fg_blue, beautiful.widget_icon_charge)
            end
            update_widget(widget, icon, bat_now.perc)
        end
    })
    return mybat
end

function widgets.net_widget()
    net_widget = lain.widget.net{
        wifi_state = "on",
        eth_state = "on",
        settings = function()
            local lan = net_now.devices.enp2s0
            local wlan = net_now.devices.wlp1s0
            if lan then
                if lan.ethernet then
                    update_widget(widget, '', "lan")
                else
                    update_widget(widget, '', '.')
                end
            end
            if wlan then
                if wlan.wifi then
                    update_widget(widget, beautiful.widget_icon_wifi, "")
                else
                    update_widget(widget, "", "no wifi")
                end
            end
        end
    }
    return net_widget
end

function widgets.weather_widget()
    myweather = lain.widget.weather({
        city_id = 2950159, -- Berlin
        lang = DE,
        units = metric,
        followmouse = true,
        settings = function()
            descr = weather_now["weather"][1]["description"]
            units = math.floor(weather_now["main"]["temp"])
            if units > 30 then
                temp_color = markup(beautiful.widget_fg_red, units)
            elseif units > 15 then
                temp_color = markup(beautiful.widget_fg_green, units)
            else
                temp_color = markup(beautiful.widget_fg_blue, units)
            end
            update_widget(widget, "Berlin: ", temp_color .. " °C ")
        end
    })
    return myweather
end

function widgets.pkg_widget()
    local pkg_widget = wibox.widget.textbox()
    pkg_widget:set_markup(beautiful.widget_icon_pkg)
    awful.widget.watch('bash -c "checkupdates | wc -l"', 3600, function(widget, stdout)
        update_widget(pkg_widget,beautiful.widget_icon_pkg,stdout .. " ")
    end)
    return pkg_widget
end

function widgets.spotify()
    local spotify_widget = wibox.widget.textbox()
    dbus.add_match("session", "path='/org/mpris/MediaPlayer2',interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'")
    dbus.connect_signal("org.freedesktop.DBus.Properties", function() spotify_widget.text = "now playing" end)
    return spotify_widget
end

function widgets.arc_graph()

    chart = wibox.widget{
        widget = wibox.container.arcchart,
        min_value = 0,
        max_value = 100,
        forced_height = 20,
        forced_width = 20,
        thickness = 2
    }
    cpu_widget = lain.widget.cpu({
        settings = function()
            chart.value = cpu_now.usage
        end
    })
    return chart
end

return widgets
