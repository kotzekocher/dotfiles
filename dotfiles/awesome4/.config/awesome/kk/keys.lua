local awful   = require("awful")
local naughty = require("naughty")
local tinsert = table.insert
modkey = "Mod4"
rofi_cmd = "rofi -show run"
rofi_layout_cmd = "rofi -show monitorlayout"


globalkeys = {}
function globalkey(mod, key, func, desc)
   local key = awful.key(mod, key, func, desc)
   for k,v in pairs(key) do
      tinsert(globalkeys, v)
   end
end

clientkeys = {}
function clientkey(mod, key, func, desc)
   local key = awful.key(mod, key, func, desc)
   for k,v in pairs(key) do
      tinsert(clientkeys, v)
   end
end

function bind_view_tag(i)
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
        tag:view_only()
    end
end

function bind_move_client_to_tag(i)
    if client.focus then
        local tag = client.focus.screen.tags[i]
        if tag then
            client.focus:move_to_tag(tag)
        end
    end
end


local function rename_tag()
    awful.prompt.run {
        prompt       = "New tag name: ",
        textbox      = awful.screen.focused().mypromptbox.widget,
        exe_callback = function(new_name)
            if not new_name or #new_name == 0 then return end

            local t = awful.screen.focused().selected_tag
            if t then
                t.name = new_name
            end
        end
    }
end

globalkey({ modkey,           }, "Escape", awful.tag.history.restore)
globalkey({ modkey,           }, "j", function () awful.client.focus.byidx( 1) end)
globalkey({ modkey,           }, "k", function () awful.client.focus.byidx(-1) end)
globalkey({ modkey,           }, "w", function () mainmenu:show() end)
-- Layout manipulation
globalkey({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end)
globalkey({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end)
globalkey({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end)
globalkey({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end)
globalkey({ modkey,           }, "u", awful.client.urgent.jumpto)
-- Standard program
globalkey({ modkey,           }, "Return", function () awful.spawn(terminal) end)
globalkey({ modkey, "Control" }, "r", awesome.restart)
globalkey({ modkey, "Shift"   }, "q", awesome.quit)
globalkey({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end)
globalkey({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end)
globalkey({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end)
globalkey({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end)
globalkey({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end)
globalkey({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end)
globalkey({ modkey,           }, "space", function () awful.layout.inc( 1)                end)
globalkey({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end)
globalkey({ modkey }, "p", function() awful.util.spawn(rofi_cmd) end)
globalkey({ modkey, "Shift" }, "p", function() awful.util.spawn(rofi_layout_cmd) end)
globalkey({ modkey }, "m", function() awful.util.spawn("ttp.sh") end, { description = "touchpad", group = "utility" })
globalkey({ modkey }, "s", function() awful.util.spawn(lock_command) end, { description = "lock", group = "utility" })
globalkey({ modkey, "Shift" }, "F2", rename_tag, { description = "rename current tag", group = "utility" })
globalkey({ }, "Print", function() awful.util.spawn("scrot '%Y-%m-%d_$wx$h_scrot.png' -e 'mv $f ~/Bilder'") end)
globalkey({ "Shift" }, "Print",
            function()
                local cmd = "sleep 0.5 && scrot -s '%Y-%m-%d_$wx$h_scrot.png' -e 'mv $f ~/Bilder'"
                naughty.notify({ text = '> select are for screenshot'})
                awful.spawn.with_shell(cmd)
            end)
globalkey({ "Mod1" }, "Tab",
            function()
                clientmenu = awful.menu.clients({ theme = { width = 250 } })
            end)
globalkey({}, "XF86AudioPlay", function() awful.util.spawn("playerctl play-pause", false) end)
globalkey({}, "XF86AudioNext", function() awful.util.spawn("playerctl next", false) end)
globalkey({}, "XF86AudioPrev", function() awful.util.spawn("playerctl previous", false) end)
globalkey({}, "XF86AudioMute", function() awful.util.spawn("amixer -D pulse set Master 1+ toggle", false) end)

globalkey({}, "XF86AudioLowerVolume", function() awful.util.spawn("pactl set-sink-volume 0 -5%", false) end)
globalkey({}, "XF86AudioRaiseVolume", function() awful.util.spawn("pactl set-sink-volume 0 +5%", false) end)
clientkey({ modkey,           }, "f",
    function (c)
        c.fullscreen = not c.fullscreen
        c:raise()
    end)
clientkey({ modkey, "Shift"   }, "c",      function (c) c:kill() end)
clientkey({ modkey, "Control" }, "space",  awful.client.floating.toggle )
clientkey({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end)
clientkey({ modkey,           }, "o",      function (c) c:move_to_screen() end)
clientkey({ modkey,           }, "t",      function (c) c.ontop = not c.ontop end)
clientkey({ modkey,           }, "n", function (c) c.minimized = true end)
clientkey({ modkey,           }, ".",
    function (c)
        c.maximized = not c.maximized
        c.maximized_horizontal = false
        c.maximized_vertical = false
        c.floating = false
        c:raise()
    end)

for i = 1, 9 do
    globalkey({ modkey }, "#" .. i + 9, function () bind_view_tag(i) end)
    globalkey({ modkey, "Shift" }, "#" .. i + 9, function() bind_move_client_to_tag(i) end)
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))

taglist_buttons = awful.util.table.join(
    awful.button({ }, 1, function(t) t:view_only() end)
)

tasklist_buttons = awful.util.table.join(
  awful.button({ }, 1, function (c)
      if c == client.focus then
	c.minimized = true
      else
	c.minimized = false
	if not c:isvisible() and c.first_tag then
	  c.first_tag:view_only()
	end
	client.focus = c
	c:raise()
      end
  end),
  awful.button({ }, 2, function(c)
      local tbar_menu_items = {
	{ "close", function() c:kill() end },
	{ "toggle floating", function() c.floating = not c.floating end },
	{ "toggle maximize", function() c.maximized = not c.maximized end },
	{ "toggle ontop", function() c.ontop = not c.ontop end },
	{ "toggle sticky", function() c.sticky = not c.sticky end },
      }
      local tbar_menu = awful.menu({items = tbar_menu_items})
      client.focus = c
      c:raise()
      tbar_menu:toggle()
  end)
)

root.keys(globalkeys)
