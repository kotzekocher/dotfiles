local awful     = require "awful"
local beautiful = require "beautiful"

awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     size_hints_honor = false,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = { "Arandr", "Pavucontrol", "Steam", "PlayOnLinux", "Teamspeak 3", "Wine", "xtightvncviewer", "Pidgin", "skypeforlinux", "VirtualBox", "Remmina", "Virt-manager", "Synergy", },
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = { type = { "normal", "dialog" },
      }, properties = { titlebars_enabled = true }
    },
    -- this can be global, because there is no point in not doing so
    { rule_any = {
        class = {"Firefox", "Google-chrome", "Steam", "Gedit", "Wine", "Eog", "LolClient.exe", "Thunderbird", "Wps", "Wrapper-1.0", "Xfce4-terminal", "URxvt", "XTerm", "Chromium", "Evince", "Nautilus", "Meld", "File-roller", "Nightly", "Emacs", "kitty", "Alacritty"},
        name = {"NeovimGtk"}
      }, properties = { titlebars_enabled = false }
    },
}
