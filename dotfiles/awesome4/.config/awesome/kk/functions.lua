local awful     = require "awful"
local beautiful = require "beautiful"
local gears     = require "gears"
local gtable    = require "gears.table"
local cairo     = require "lgi".cairo
local wibox     = require "wibox"

functions = {}

string.startswith = function(self, str)
    return self:find('^' .. str) ~= nil
end

function functions.update_my_layoutbox(l, s)
    local screen = s or 1
    local layoutname = beautiful["layout_txt_" .. awful.layout.getname(awful.layout.get(screen))]
    l:set_markup(markup(beautiful.xrdb.color0, layoutname))
end

function functions.set_wallpaper(s)
    if beautiful.wallpapers then
        local wallpaper = beautiful.wallpapers[s.index]
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        if type(wallpaper) == "string" then
            if wallpaper:startswith('#') then
                gears.wallpaper.set(wallpaper)
            end
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

function functions.get_icon(name)
    return awful.util.geticonpath(name, {'png'}, {"/usr/share/icons/Moka/"}, 64)
end

function functions.powerround(cr,width,height,radius)
    radius = radius or 10
    cr:move_to(0,0)
    if radius < 0 then
        cr:line_to(width, 0)
        cr:curve_to(width, 0, width + radius, height / 2, width, height)
        cr:line_to(0 + (radius*-1), height)
        cr:curve_to(0 + (radius*-1), height, 0, height / 2, 0 + (radius*-1), 0)
    else
        cr:line_to(width - radius, 0)
        cr:curve_to(width - radius, 0, width, height / 2, width - radius, height)
        cr:line_to(0, height)
        cr:curve_to(0, height, radius, height / 2, 0, 0)
    end
    cr:close_path()
end

function functions.my_tags(names, s, templates)
    -- mimic tag() function, except for templates
    local tags = {}
    for id,tag in ipairs(names) do
        if not templates[id] then
            templates[id] = { layout = awful.layout.suit.tile, gap_single_client = false }
        end
        tpl = gtable.clone(templates[id])
        tpl.screen = s
        table.insert(tags, awful.tag.add(tag, tpl))
        if id == 1 then
            tags[id].selected = true
        end
    end
    return tags
end

function functions.get_grid(width, height, step, bg, fg)
    local img = cairo.ImageSurface(cairo.Format.ARGB32, width, height)
    local cr = cairo.Context(img)
    cr:set_source(gears.color(bg))
    cr:paint()
    cr:set_source(gears.color(fg))
    xcount = math.floor(width / step)
    ycount = math.floor(height / step)
    for x=1,xcount-1 do
        cr:move_to(x*step,0)
        cr:line_to(x*step,height)
        cr:stroke()
    end
    for y=1,ycount-1 do
        cr:move_to(0,y*step)
        cr:line_to(width,y*step)
        cr:stroke()
    end
    return img
end

function functions.sep_top(width,height,len,bg,fg)
    local line_img = cairo.ImageSurface(cairo.Format.ARGB32, width, height)
    local cr = cairo.Context(line_img)
    cr:set_source(gears.color(bg))
    cr:paint()
    cr:set_source(gears.color(fg))
    cr:move_to(0,len)
    cr:line_to(0,0)
    cr:line_to(len,0)
    cr:stroke()
    cr:move_to(width - len,0)
    cr:line_to(width,0)
    cr:line_to(width,len)
    cr:stroke()
    return line_img
end

function functions.sep_top_box(w,h,l,bg,fg)
    w = wibox.widget {
        widget = wibox.widget.imagebox,
        image = functions.sep_top(w,h,l,bg,fg),
    }
    return w
end

function functions.sep_bot_box(w,h,l,bg,fg)
    w = wibox.widget {
            widget = wibox.container.mirror,
            reflection = { vertical = true },
            {
                widget = wibox.widget.imagebox,
                image = functions.sep_top(w,h,l,bg,fg),
            }
        }
    return w
end

return functions
