#env
#typeset -a path

#path=(~/bin ~/.local/bin $path[@])
export PATH=~/bin:~/.local/bin:$PATH

if [ $(hostnamectl --static status) = "droppnix" ]; then
    export PATH=~/.gem/ruby/2.3.0/bin:~/.gem/ruby/2.4.0/bin:~/code/dev-dialog/product-release-buildtooling:~/code/dev-dimensions/k8terer/bin:~/code/dev-dialog/product-release-buildtooling/k8s:~/code/dev-dialog/plato-scripts:${KREW_ROOT:-$HOME/.krew}/bin:$PATH
    #export PYTHONPATH="/home/cdr/code/dev-dialog/product-release-buildtooling/build:$PYTHONPATH"
    export NFSHOME="/home/cdr-nfs"
    export KEEPASS_DB="$HOME/newtec/Passwords.kdbx"
    export TERMINAL="tilix"
    export AWS_PROFILE=idirect-cloud
    export AWS_REGION=us-east-1
    export QT_AUTO_SCREEN_SCALE_FACTOR=0
    export QT_STYLE_OVERRIDE=gtk2
    export DIFFPROG=meld
    export MOZ_ENABLE_WAYLAND=1
    export XDG_CURRENT_DESKTOP=sway
    fpath=($fpath ~/.zsh/completion)
elif [ $(hostnamectl --static status) = "odin" ]; then
    path=(~/.gem/ruby/2.3.0/bin $path[@])
    export KEEPASS_DB="$HOME/syncthing/kp/Passwords.kdbx"
    export VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/radeon_icd.x86_64.json
elif [ $(hostnamectl --static status) = "bender" ]; then
    path=($path[@])
    export KEEPASS_DB="$HOME/syncthing/kp/Passwords.kdbx"
elif [ $(hostnamectl --static status) = "loki" ]; then
    path=($path[@])
    export KEEPASS_DB="$HOME/syncthing/kp/Passwords.kdbx"
elif [ $(hostnamectl --static status) = "thor" ]; then
    path=(~/scripts $path[@])
    export TERM=xterm-256color
fi

export NNN_USE_EDITOR=1
