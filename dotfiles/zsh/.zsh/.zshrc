# load stuff
autoload -U compinit && compinit
autoload -U promptinit && promptinit
autoload -U colors && colors
autoload -Uz vcs_info

zstyle ':vcs_info:*' formats '%F{green}%s%f %F{red}[%b]%f %F{yellow}%m%u%c%f'
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b:%r'

zstyle ':completion:*' menu select=2
zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;31"
zstyle ":completion:*:kill:*" command "ps -u $USER -o pid,%cpu,tty,cputime,cmd"
zstyle ':completion:*' rehash true
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

setopt completealiases
setopt AUTO_PUSHD
setopt AUTO_CD

bindkey "^r" history-incremental-pattern-search-backward
bindkey "^t" history-incremental-pattern-search-forward

bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word

#HISTORY
export HISTSIZE=100000000
export SAVEHIST=$HISTSIZE
export HISTFILE=$ZDOTDIR/.zsh_history
export HISTTIMEFORMAT="[%F %T] "
setopt share_history
setopt HIST_IGNORE_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt INC_APPEND_HISTORY_TIME
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY

# Environment variables
HOSTNAME=$(hostnamectl --static status)
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20
export EDITOR=nvim
export VISUAL=nvim
set COLORFGBG="green;black"
export COLORFGBG
export ALTERNATE_EDITOR=""
export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_DEFAULT_OPTS="--layout=reverse --inline-info"
export FZF_CTRL_T_OPTS="
  --preview 'bat -n --color=always {}'
  --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_CTRL_R_OPTS="
  --preview 'echo {}' --preview-window up:3:hidden:wrap
  --bind 'ctrl-/:toggle-preview'
  --bind 'ctrl-y:execute-silent(echo -n {2..} | pbcopy)+abort'
  --color header:italic
  --header 'Press CTRL-Y to copy command into clipboard'"
export FZF_ALT_C_OPTS="--preview 'tree -C {}'"

if [ -x "$(command -v kubectl)" ]; then . <(kubectl completion zsh); fi
if [ -x "$(command -v oc)" ]; then . <(oc completion zsh); fi
if [ -x "$(command -v crc)" ]; then . <(crc completion zsh); fi

if [ -n "$PYTHONPATH" ]; then
    export PYTHONPATH='/usr/lib/python3.10/site-packages/pdm/pep582':$PYTHONPATH
else
    export PYTHONPATH='/usr/lib/python3.10/site-packages/pdm/pep582'
fi


#############
# "Plugins" #
#############

to_source=(
  "$ZDOTDIR/zkdb.zsh"
  "$ZDOTDIR/.zsh_aliases"
  "/usr/share/doc/pkgfile/command-not-found.zsh"
  "$HOME/code/repo/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
  "$HOME/code/repo/zsh-autosuggestions/zsh-autosuggestions.zsh"
  "$HOME/code/repo/zsh-z/zsh-z.plugin.zsh"
  "/usr/share/fzf/key-bindings.zsh"
  "$HOME/.config/broot/launcher/bash/br"
  "/etc/profile.d/vte.sh"
)

for f in ${to_source}; do
	if [ -f ${f} ]; then
		source ${f}
	fi
done

##########
# Prompt #
##########

_prompt_retcode() {
	if [ $? -eq 0 ]; then
		rc="%{$fg_bold[green]%}$?%{$reset_color%}"
	else
		rc="%{$fg_bold[yellow]%}$?%{$reset_color%}"
	fi
	echo "[${rc}]"
}

_prompt_ssh() {
	if [ ! -z "$SSH_CONNECTION" ]; then
        echo "%{$fg_bold[yellow]%}(${HOSTNAME})%{$reset_color%}:"
	fi
}

_prompt_dir() {
    echo "%{$fg_bold[green]%}$(shpwd)%{$reset_color%}"
}

_prompt_separator() {
	echo "%{$fg_bold[red]%}>%{$reset_color%}"
}

setopt PROMPT_SUBST
PROMPT='$(_prompt_ssh)$(_prompt_dir) $(_prompt_separator) '
RPROMPT='${vcs_info_msg_0_}$(_prompt_retcode)'

_term_title() {
	print -Pn "\e]0;zsh%L %(1j,%j job%(2j|s|); ,)%~\a"
}
_term_title_cmd() {
	printf "\033]0;%s\a" "$1"
}

_jira_completion() {
  eval $(env _TYPER_COMPLETE_ARGS="${words[1,$CURRENT]}" _JIRA_COMPLETE=complete_zsh jira)
}

_fill_timesheetspy_completion() {
  eval $(env _TYPER_COMPLETE_ARGS="${words[1,$CURRENT]}" _FILL_TIMESHEETS.PY_COMPLETE=complete_zsh fill_timesheets.py)
}

compdef _jira_completion jira
compdef _fill_timesheetspy_completion fill_timesheets.py

add-zsh-hook precmd vcs_info
add-zsh-hook precmd _term_title
add-zsh-hook preexec _term_title_cmd

source /home/cdr/.config/broot/launcher/bash/br
