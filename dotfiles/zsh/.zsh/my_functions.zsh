#!/bin/env zsh

#ssh() {
#    my_ssh.sh $@
#}

qr_code() {
    import -silent -window root bmp:- | zbarimg -q -
}

venv() {
    # load pyhon venv
    envs=$(find ~/code/python_venv/ ~/.local/share/virtualenvs -mindepth 1 -maxdepth 1 -type d)
    virtenv=$(echo -e $envs | fzf)
    source "${virtenv}/bin/activate"
}

man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;32m") \
            man "$@"
}

c() {
    for C in {0..255}; do
        tput setab $C
        echo -n "$C "
       [[ $((C % 16)) -eq 0 ]] && echo -e "\\n"
    done
    tput sgr0
    echo
}

new() {
    local fn=${1}
    local ext=${fn##*\.}
    declare -A hdr=([sh]="#!/bin/bash\n\n" [py]="#!/usr/bin/env python\n\n")
    if [ ! -e ${fn} ]; then
        echo ${hdr[$ext]} > ${fn}
        chmod +x ${fn}
        nvim + ${fn}
    else
        nvim + ${fn}
    fi
}

shpwd() {
  echo ${${:-/${(j:/:)${(M)${(s:/:)${(D)PWD:h}}#(|.)[^.]}}/${PWD:t}}//\/~/\~}
}

clean-ssh-key() {
    local line="${1}"
    if [ ! -e "${line}" ]; then
        sed -i "${line} d" ~/.ssh/known_hosts
    fi
}

run_config_minikube() {
    local version=${1:-"v1.19.9"}
    minikube start \
        --cni calico \
        --memory $(( 15 * 1024 )) \
        --cpus "8" \
        --vm-driver="docker" \
        --kubernetes-version="${version}"
    minikube addons enable registry
    minikube addons enable metrics-server
    minikube addons enable dashboard
    minikube addons enable metallb
    kubectl get configmaps config -n metallb-system -o json | jq '.data.config = "address-pools:\n- name: default\n  protocol: layer2\n  addresses:\n  - 192.168.49.100-192.168.49.200\n"' | kubectl replace -f -

    docker run --rm -it -d --network=host alpine ash -c "apk add socat && socat TCP-LISTEN:5000,reuseaddr,fork TCP:$(minikube ip):5000"
}


pacsearch() {
    pacman -Slq | fzf --multi --preview 'pacman -Si {1}' | xargs -ro sudo pacman -S
}

yaysearch() {
    yay -Slq | fzf --multi --preview 'yay -Si {1}' | xargs -ro yay -S
}

pacuninstall() {
    pacman -Qq | fzf --multi --preview 'pacman -Qi {1}' | xargs -ro sudo pacman -Rns
}
