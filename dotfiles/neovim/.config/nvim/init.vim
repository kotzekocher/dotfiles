set nocompatible
filetype off

set t_Co=256

function! DoRemote(arg)
  UpdateRemotePlugins
endfunction

call plug#begin('~/.config/nvim/plugged')
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
Plug 'zchee/deoplete-jedi'
Plug 'fishbullet/deoplete-ruby'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-syntastic/syntastic'
Plug 'tpope/vim-fugitive'
Plug 'chase/vim-ansible-yaml'
Plug 'puppetlabs/puppet-syntax-vim'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'ap/vim-css-color'
Plug 'vimwiki/vimwiki'
Plug 'baabelfish/nvim-nim'
Plug 'altercation/vim-colors-solarized'
call plug#end()

set clipboard=unnamed
set clipboard+=unnamedplus

syntax enable
set nocompatible

" auto indent "
set autoindent

set colorcolumn=120
highlight ColorColumn ctermbg=8 guibg=8

set hlsearch
set title
set wildmenu
set smartindent
set laststatus=2
set showmatch

set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END


" 1 tab = 4 spaces "
set softtabstop=0
set tabstop=4
set shiftwidth=4

" set expandtab
" underline "
set cursorline
"set cursorcolumn
"highlight cursorcolumn ctermbg=8 guibg=8

"set incsearch
set ignorecase
set smartcase

filetype plugin on
autocmd FileType html setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType javascript setlocal shiftwidth=4 tabstop=4 expandtab
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2 expandtab
autocmd FileType eruby setlocal expandtab shiftwidth=2 tabstop=2 nosmartindent noautoindent
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType lua setlocal expandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType sh setlocal expandtab shiftwidth=4 softtabstop=4
autocmd FileType zsh setlocal expandtab shiftwidth=4 softtabstop=4
autocmd FileType make setlocal noexpandtab tabstop=4
autocmd FileType puppet setlocal expandtab shiftwidth=2 tabstop=2
autocmd BufNewFile,BufRead *.yaml,*.yml set filetype=ansible

" remove trailing spaces on save
autocmd BufWritePre * :%s/\s\+$//e

" NERDTree
autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

map <C-n> :NERDTreeToggle<CR>

" NERDcomment
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'

inoremap <Nul> <C-x><C-o>
set pastetoggle=<F5>

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme = 'ayu_mirage'

let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#jedi#show_docstring = 0

set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_sh_shellcheck_args="-e SC1090,SC1091"
let g:syntastic_mode_map = { 'passive_filetypes': ['python'] }
tnoremap <Esc> <C-\><C-n>

let g:vimwiki_list = [{'path': '~/projekte/vimwiki/', 'path_html': '~/public_html/wiki'}]
