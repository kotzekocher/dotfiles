#!/bin/bash
#polybar-msg cmd quit
#sleep 1

killall -q polybar
i=0
while pgrep -x polybar >/dev/null; do
    sleep 1;
    i=$((i+1))
    if [[ $i -eq 2 ]]; then
        killall -q -s 9 polybar
    fi
done

echo "--- restart ---" | tee -a /tmp/polybar1.log /tmp/polybar2.log /tmp/polybar3.log

case ${HOSTNAME} in
  droppnix)
    if type "xrandr"; then
      for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
        if [[ $m == "eDP1" ]]; then
          bar='droppnix-main'
        else
          bar='droppnix-norm'
        fi
        MONITOR=$m polybar --reload ${bar} 2>&1 | tee -a /tmp/polybar-${bar}.log & disown
      done
    else
      polybar --reload droppnix-1 2>&1 | tee -a /tmp/polybar1.log & disown
    fi
    ;;
esac
