;;; package --- Summary:
;;; Commentary:
;;; omg

;;; Code:
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

(eval-when-compile
  (require 'use-package))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package company
  :ensure t
  :diminish
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (setq company-idle-delay 0.1)
  (setq company-show-numbers t)
  (setq company-minimum-prefix-length 2)
  (setq company-dabbrev-code-other-buffers 'all)
  (setq company-dabbrev-code-everywhere t)
  (setq company-dabbrev-code-ignore-case t)
  (add-to-list 'company-backends 'company-web-html)
  (add-to-list 'company-backends 'company-web-jade)
  (add-to-list 'company-backends 'company-web-slim)
  (use-package company-anaconda
    :ensure t
    :config
    (add-to-list 'company-backends 'company-anaconda)))

(add-hook 'after-init-hook #'global-flycheck-mode)

(with-eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook 'flycheck-popup-tip-mode))

(add-hook 'racer-mode-hook #'company-mode)
(require 'rust-mode)
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(setq company-tooltip-align-annotations t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-enabled-themes (quote (dracula)))
 '(custom-safe-themes
   (quote
    ("aaffceb9b0f539b6ad6becb8e96a04f2140c8faa1de8039a343a4f1e009174fb" "3fa81193ab414a4d54cde427c2662337c2cab5dd4eb17ffff0d90bca97581eb6" "7366916327c60fdf17b53b4ac7f565866c38e1b4a27345fe7facbf16b7a4e9e8" "a566448baba25f48e1833d86807b77876a899fc0c3d33394094cf267c970749f" "b4c13d25b1f9f66eb769e05889ee000f89d64b089f96851b6da643cee4fdab08" "9d9fda57c476672acd8c6efeb9dc801abea906634575ad2c7688d055878e69d6" "42b9d85321f5a152a6aef0cc8173e701f572175d6711361955ecfb4943fe93af" "ed0b4fc082715fc1d6a547650752cd8ec76c400ef72eb159543db1770a27caa7" default)))
 '(font-use-system-font t)
 '(global-linum-mode t)
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
    (dracula-theme flycheck-popup-tip flycheck ace-window racer company-box doom-themes company-web pythonic company-anaconda python-mode company-lua ## company-jedi company kivy-mode dashboard telephone-line gruvbox-theme use-package)))
 '(scroll-bar-mode nil)
 '(setq make-backup-files nil)
 '(setq indent-line-function t)
 '(setq-default tab-width t)
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(setq make-backup-files nil)
 '(setq backup-inhibited t)
 '(setq auto-save-default nil)
 '(setq tramp-default-method "ssh")
 '(vc-follow-symlinks nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(use-package telephone-line
  :config
  (telephone-line-mode 1))

(use-package dashboard
  :bind (
	 :map dashboard-mode-map
	      ("<down-mouse-1>" . nil)
	      ("<mouse-1>" . widget-button-click)
	      ("<mouse-2>" . widget-button-click))
  :config
  (setq dashboard-banner-logo-title "Emacs")
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-items '((recents  . 5)
			  (bookmarks . 5)
			  (agenda . 5)))
  (dashboard-setup-startup-hook))

;; org-mode-stuff
(setq org-log-done 'time
      org-agenda-files (list "~/org" "~/org/phone-orgzly")
      org-tags-column 80
      org-startup-indented t)

;(defun my/python-mode-hook ()
;  (add-to-list 'company-backends 'company-jedi))

;(use-package python
;  :config
;  (add-hook 'python-mode-hook 'my/python-mode-hook)
;  )

(use-package anaconda-mode
  :ensure t
  :config
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode))

(use-package kivy-mode
  :ensure t
  :mode ("\\.kv\\'" . kivy-mode))

(use-package lua-mode
  :config
  (setq lua-indent-level 2)
  (require 'company-lua)
  )

(global-set-key (kbd "M-o") 'ace-window)
(load "server")
(unless (server-running-p) (server-start))
