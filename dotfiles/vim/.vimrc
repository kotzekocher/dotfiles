set nocompatible
filetype off

set t_Co=256

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Bundle 'vim-ruby/vim-ruby'
Plugin 'vim-airline/vim-airline'
call vundle#end()
filetype plugin indent on

syntax enable

set nocompatible

" auto indent "
set autoindent

"set colorcolumn=80

set hlsearch
set title
set wildmenu
"set wildmode=list:longest
set smartindent
set laststatus=2
"set number
set relativenumber
set showmatch

" 1 tab = 4 spaces "
set softtabstop=0
set tabstop=4
set shiftwidth=4

" set expandtab
" underline "
set cursorline

"set incsearch
set ignorecase
set smartcase

autocmd FileType html setlocal shiftwidth=2 tabstop=2
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType lua setlocal expandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd FileType sh setlocal expandtab shiftwidth=4 softtabstop=4
autocmd FileType make setlocal noexpandtab tabstop=4

" remove trailing spaces on save
autocmd BufWritePre * :%s/\s\+$//e

filetype plugin on
set ofu=syntaxcomplete#Complete
inoremap <Nul> <C-x><C-o>
set pastetoggle=<F5>

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'


let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

"nnoremap <up> :buffers<cr>:buffer
"nnoremap <down> :b#<cr>
"nnoremap <left> :bp<cr>
"nnoremap <right> :bn<cr>
