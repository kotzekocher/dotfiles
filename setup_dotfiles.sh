#!/bin/bash

set -e

TARGET_DIR="$HOME/"
STOW_DIR="$HOME/code/repo/dotfiles/dotfiles"
HOSTNAME=$(hostname --short)
export STOW_DIR

# configs
_init_for_droppnix() {
    pkgs="neovim rofi scripts sway environment.d zsh"
}

_init_for_bender() {
    pkgs="awesome4 i3 neovim rofi scripts xresources zsh emacs kitty offlineimap xinit neomutt tmux dunst mbsync"
}

_init_for_odin() {
    pkgs="neovim rofi scripts xresources zsh tmux i3-gaps neomutt dunst polybar mbsync alacritty"
}

_init_for_thor() {
    pkgs="neovim scripts zsh tmux"
}

_init_for_loki() {
    pkgs="neovim scripts zsh tmux neomutt mbsync offlineimap rofi alacritty i3-gaps polybar dunst sway"
}

# functions
_log() {
    lvl=$1; shift
    case $lvl in
        info)color=2;;
        warn)color=3;;
        err)color=1;;
        *)color=2;;
    esac
    echo -e "$(tput bold)$(tput setaf $color)> $*$(tput sgr0)"
}
_setup_external() {
    # TODO: refactor this
    git submodule update --init
    # external repositories
    cd "${HOME}/code/repo" || exit 1
    if [ ! -d zsh-syntax-highlighting ]; then
        git clone https://github.com/zsh-users/zsh-syntax-highlighting
    else
        pushd zsh-syntax-highlighting; git pull; popd
    fi
    if [ ! -d zsh-autosuggestions ]; then
        git clone https://github.com/zsh-users/zsh-autosuggestions
    else
        pushd zsh-autosuggestions; git pull; popd
    fi
    if [ ! -d "zsh-z" ]; then
        git clone https://github.com/agkozak/zsh-z
    else
        pushd zsh-z; git pull; popd
    fi
    curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

_usage() {
    echo "$0 < install | uninstall | update | external >"
    exit 1
}

_do() {
    action=${1}
    case ${1} in
        install)action="-S";;
        update)action="-R";;
        uninstall)action="-D";;
        external)_setup_external; exit 0;;
        *)_log err "unknown action"; _usage;;
    esac
    for f in ${pkgs}; do
        _log info "processing ${f}"
        stow -t "${TARGET_DIR}" "${action}" "${f}"
    done
}

# main
_init_for_"${HOSTNAME}"

while [[ -n "${1}" ]]; do
    case $1 in
        --pkgs)
            pkgs="${2}"
            shift;;
        --help|-h)_usage;;
        *)
            if [[ $# -eq 1 ]]; then
                _do "${1}"
            else
                _log err "wrong number of arguments"
                _usage
            fi
    esac
    shift
done
