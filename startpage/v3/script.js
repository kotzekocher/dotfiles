
function startTime(){
	var today = new Date();
	var h = checkTime(today.getHours());
	var m = checkTime(today.getMinutes());
	var s = checkTime(today.getSeconds());
	var d = today.getDate();
	var M = today.getMonth() + 1;
	var y = today.getFullYear();
	document.getElementById('time').innerHTML = h + "<span>:</span>" + m;
	document.getElementById('seconds').innerHTML = ""+s;
	//document.getElementById('date').innerHTML = d + "." + M + "." + y;
	var t = setTimeout(startTime, 1000);
}
function checkTime(i) {
	if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	return i;
}

function init() {
	startTime();
}

document.addEventListener('DOMContentLoaded', function () {
	init();
});
