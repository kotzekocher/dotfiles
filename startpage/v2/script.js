
function startTime(){
	var today = new Date();
	var h = checkTime(today.getHours());
	var m = checkTime(today.getMinutes());
	var s = checkTime(today.getSeconds());
	var d = today.getDate();
	var M = today.getMonth() + 1;
	var y = today.getFullYear();
	document.getElementById('time').innerHTML = h + "<span>:</span>" + m;
	//document.getElementById('date').innerHTML = d + "." + M + "." + y;
	var t = setTimeout(startTime, 2000);
}
function checkTime(i) {
	if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	return i;
}

function loadJSON(path, success, error)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success)
                    success(JSON.parse(xhr.responseText));
            } else {
                if (error)
                    error(xhr);
            }
        }
    };
    xhr.open("GET", path, true);
    xhr.send();
}

function get_link_obj(desc, link) {
	var element = document.createElement("a");
	element.setAttribute("href", link);
	element.innerHTML = desc;
	return element
}

function parse_links(data) {
	console.log(data.top);
	for (var k in data.top) {
		var node = document.createElement("LI");
		document.getElementById("toplinks").appendChild(node);
		node.appendChild(get_link_obj(k, data.top[k]));
		console.log(k, data.top[k]);
	}
	for (var i in data.links) {
		console.log(i, data.links[i]);
		var ul = document.createElement("UL");
		document.getElementById("links1").appendChild(ul);
		ul.setAttribute("class", "links");
		for (k in data.links[i]) {
			var node = document.createElement("LI");
			node.appendChild(get_link_obj(k, data.links[i][k]));
			ul.appendChild(node);
		}
	}
}

function init() {
	startTime();
	loadJSON("links.json",
		function(data) { parse_links(data); },
		function(xhr){console.log("error")}
	)
	var editor = document.querySelector("#editor");
	if (window.localStorage["TextEditorData"]) {
	    editor.value = window.localStorage["TextEditorData"];
	}
	editor.addEventListener("keyup", function() {
	    window.localStorage["TextEditorData"] = editor.value;
	});
}

document.addEventListener('DOMContentLoaded', function () {
	init();
});
