#!/bin/bash

src="${1%/}"
dst="startpage-${src}.xpi"

if [ ! -z "${src}" ]; then
    zip -j "${dst}" "${src}"/*
else
    echo "no directory given"
fi
