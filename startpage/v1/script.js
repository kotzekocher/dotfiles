
function startTime(){
	var today = new Date();
	var h = checkTime(today.getHours());
	var m = checkTime(today.getMinutes());
	var s = checkTime(today.getSeconds());
	var d = today.getDate();
	var M = today.getMonth() + 1;
	var y = today.getFullYear();
	document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
	document.getElementById('date').innerHTML = d + "." + M + "." + y;
	var t = setTimeout(startTime, 500);
}
function checkTime(i) {
	if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	return i;
}
function load_stuff(file, id) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			document.getElementById(id).innerHTML = xhttp.responseText;
		} else {
			document.getElementById(id).innerHTML = "no data for \"#" + id + "\"";
		}
	}
	bla=Math.random()*10000000000000000;
	xhttp.open("GET", file + "?bla=" + bla, true);
	xhttp.send();
}
function init() {
	startTime();
}

document.addEventListener('DOMContentLoaded', function () {
	init();
});
