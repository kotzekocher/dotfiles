#!/bin/bash

set -eE
#set -x
#set -o errtrace


BACKUP_PATH='/srv/storage-backup'
DISK_LABEL='storage-backup'
LOGFILE=''
PRUNE_AFTER=0

# borg init --encryption=none backup_containers
# borg init --encryption=none backup_http
# borg init --encryption=none backup_storage

# config file
# shellcheck source=./backup.conf
source /srv/storage/chris/scripts/backup.conf

trap _cleanup ERR

_cleanup() {
    _log err "script exited with error. restore machine state."
    _start_lxc
    _unmount
}

_debug_init() {
    exec 2> /var/log/backup_dbg.log
    set -x
}

_stop_lxc() {
    if lxc-autostart --ignore-auto --shutdown --timeout=60; then
        _log info "stopped lxc containers"
    fi
}

_start_lxc() {
    if lxc-autostart --all; then
        _log info "started lxc containers"
    fi
}

_sleep_disk() {
    # put disk in standby mode (~6W less power consumption)
    disk=$(realpath /dev/disk/by-label/${DISK_LABEL})
    _log info "putting disk $disk in standby mode"
    hdparm -y "${disk}"
}

_log() {
    lvl=$1; shift
    case $lvl in
        info)color=32;;
        warn)color=33;;
        err)color=31;;
        *)color=32;;
    esac
    echo -e "\e[${color}m> $*\e[0m"
    if [ -n "$LOGFILE" ]; then
        if [ "$LOGFILE" = "syslog" ]; then
            logger "$0:$*"
        else
            echo "$(date) : $*" >> "${LOGFILE}"
        fi
    fi
}

_list_backups() {
    _mount
    backups=$(find ${BACKUP_PATH} -maxdepth 1 -type d -name "backup*" -exec echo {} \;)
    _log info 'available backups'
    # shellcheck disable=SC2154
    [[ -v "$output_list_to_file" ]] || echo > "$output_list_to_file"
    for b in $backups; do
        if [[ ! -v $output_list_to_file ]]; then
            borg list "$b" | tee -a "$output_list_to_file"
        else
            borg list "$b"
        fi
    done
    _unmount
}

_create_backup() {
    repository=$1;shift
    archive=$1;shift
    content=$1
    _log info "backing up $content to $repository::$archive"
    borg create \
        --verbose \
        --filter AME \
        --list \
        --stats \
        --show-rc \
        --compression lz4 \
        --exclude-caches \
        --exclude '*lost+found*'    \
        "${BACKUP_PATH}/backup_${repository}"'::'"$archive"'-{now}' \
        "${content}"
}

_prune_backup() {
    repository=$1;shift
    prefix=$1;shift
    _log info "pruning repository ${repository} with prefix ${prefix}"
    borg prune \
        --list \
        --prefix "${prefix}" \
        --show-rc \
        --keep-daily 3 \
        --keep-weekly 2 \
        --keep-monthly 4 \
        "${BACKUP_PATH}/backup_${repository}"
}

usage() {
    echo -e "$0 [options] COMMAND"
    echo -e "options:"
    echo -e "    --logfile PATH|syslog\tlog to PATH or log to syslog"
    echo -e "    --debug\tlog debug output to /var/log"
    echo -e "    --prune-after\tprune old backups after backing up"
    echo -e "COMMAND:"
    echo -e "    list\tlist all backups and archives"
    echo -e "    backup_all\tbackup configured backups and archives"
    echo -e "    prune_all\tonly prune backups"
    echo -e "    mount\tmount backup disk"
    echo -e "    unmount\tunmount backup disk"
}

_make_backups() {
    _stop_lxc
    _mount
    # shellcheck disable=SC2154
    for backup in ${!backup_@}; do
        IFS=',' read -r -a backup_info <<< "${!backup}"
        _create_backup "${backup_info[0]}" "${backup_info[1]}" "${backup_info[2]}"
        if [ ${PRUNE_AFTER} -eq 1 ];then
            _log info "PRUNE_AFTER selected. removing some old backups.."
            _prune_backup "${backup_info[0]}" "${backup_info[1]}"
        fi
    done
    _unmount
    _start_lxc
}

_make_prune_backups() {
    _mount
    for backup in ${!backup_@}; do
        IFS=',' read -r -a backup_info <<< "${!backup}"
        _prune_backup "${backup_info[0]}" "${backup_info[1]}"
    done
    _unmount

}

_mount() {
    if ! mountpoint -q ${BACKUP_PATH}; then
        _log warn "backup volume is not mounted, mounting now..."
        if ! mount -L "${DISK_LABEL}" "${BACKUP_PATH}"; then
            _log err "mount failed"
        else
            _log info "mounted backup volume"
        fi
    fi
}

_unmount() {
    _log info "unmounting backup volume"
    if ! umount ${BACKUP_PATH}; then
        _log err "unmount failed"
    else
        _log info "unmounted backup volume"
        sleep 1
        _sleep_disk
    fi
}

## MAIN ##

if [[ $EUID -gt 0 ]]; then
    _log err 'You need to be root'
    exit 1
fi

_log info "called $0 $*"

while [[ -n "${1}" ]]; do
    case $1 in
        --logfile)LOGFILE=$2; shift;;
        --prune-after)PRUNE_AFTER=1;;
        --debug)_debug_init;;
        -h|--help)usage;shift;;
    esac
    if [[ $# -eq 1 ]]; then
        case $1 in
            list)_list_backups;;
            backup_all)_make_backups;;
            prune_all)_make_prune_backups;;
            mount)_mount;;
            unmount)_unmount;;
        esac
    fi
    shift
done
